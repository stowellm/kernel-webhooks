"""Webhook interaction tests."""
from copy import deepcopy
from unittest import TestCase
from unittest import mock

from tests import fake_payloads
from tests import fakes
from webhook import ckihook
from webhook.pipelines import PipelineType

CHANGES = {'labels': {'previous': [{'title': f'{PipelineType.REALTIME.prefix}::Running'},
                                   {'title': 'CKI::Running'},
                                   {'title': f'{PipelineType.AUTOMOTIVE.prefix}::Running'}
                                   ],
                      'current': [{'title': f'{PipelineType.REALTIME.prefix}::Canceled'}]
                      }
           }


class TestHelpers(TestCase):
    """Test helper functions."""

    def test_branch_changed(self):
        """Returns True if it seems the MR branch changed."""
        # No head pipeline ID, returns False
        payload = {'object_attributes': {'head_pipeline_id': None}}
        self.assertIs(ckihook.branch_changed(payload), False)
        # Merge status exists, returns True
        payload['object_attributes']['head_pipeline_id'] = 1
        payload['changes'] = {'merge_status': 'boop'}
        self.assertIs(ckihook.branch_changed(payload), True)
        # Merge status not found, returns False
        payload['changes'] = {}
        self.assertIs(ckihook.branch_changed(payload), False)

    def test_cki_label_changed(self):
        """Returns True if any CKI labels are in the changes list."""
        changes = deepcopy(CHANGES)
        self.assertTrue(ckihook.cki_label_changed(changes))
        self.assertFalse(ckihook.cki_label_changed({}))

    @mock.patch('webhook.ckihook.get_downstream_pipeline_branch')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_process_possible_branch_change_non_prod(self, mock_get_ds_branch):
        """Makes no changes."""
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        mock_instance = fakes.FakeGitLab()
        mock_project = mock_instance.add_project(fake_payloads.PROJECT_ID,
                                                 fake_payloads.PROJECT_PATH_WITH_NAMESPACE)
        mock_pipe = mock.Mock()

        # No pipelines, returns False.
        graph_mr = mock.Mock(gl_instance=mock_instance, gl_project=mock_project, pipelines=[])
        self.assertIs(ckihook.process_possible_branch_change(graph_mr, payload), False)

        # Branch not determined for current downstream pipeline, returns False.
        graph_mr.pipelines = [mock_pipe]
        mock_get_ds_branch.return_value = None
        self.assertIs(ckihook.process_possible_branch_change(graph_mr, payload), False)

        # Downstream pipeline 'branch' matches mr target branch, returns False.
        mock_get_ds_branch.return_value = payload['object_attributes']['target_branch']
        self.assertIs(ckihook.process_possible_branch_change(graph_mr, payload), False)

        # Downstream pipeline branch has changed, but not production, returns True.
        mock_get_ds_branch.return_value = 'new_branch'
        self.assertIs(ckihook.process_possible_branch_change(graph_mr, payload), True)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('webhook.ckihook.get_downstream_pipeline_branch')
    @mock.patch('webhook.common.cancel_pipeline')
    @mock.patch('webhook.common.create_mr_pipeline')
    def test_process_possible_branch_change_prod(self, mock_create, mock_cancel,
                                                 mock_get_ds_branch):
        """Returns True when a change is detected and pipeline is canceled/triggered."""
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        mock_instance = fakes.FakeGitLab()
        mock_project = mock_instance.add_project(fake_payloads.PROJECT_ID,
                                                 fake_payloads.PROJECT_PATH_WITH_NAMESPACE)
        mock_project.web_url = fake_payloads.PROJECT_WEB_URL

        mock_pipe = mock.Mock()
        graph_mr = mock.Mock(gl_instance=mock_instance, gl_project=mock_project,
                             pipelines=[mock_pipe], iid=fake_payloads.MR_IID,
                             head_pipeline_id=fake_payloads.MR_HEAD_PIPELINE_ID)
        mock_mr = mock_project.add_mr(graph_mr.iid)
        graph_mr.gl_mr = mock_mr

        # pipeline_branch_matches() says the branches differ so cancel/retrigger and return True
        mock_get_ds_branch.return_value = '8.8'
        self.assertIs(ckihook.process_possible_branch_change(graph_mr, payload), True)
        mock_get_ds_branch.assert_called_once_with(mock_instance, mock_pipe)
        mock_cancel.assert_called_once_with(mock_project, fake_payloads.MR_HEAD_PIPELINE_ID)
        mock_create.assert_called_once_with(mock_mr)

    def test_get_downstream_pipeline_branch(self):
        """Returns the 'branch' pipeline var value, or None."""
        pipeline_dict = {'ds_project_id': 23456, 'ds_pipeline_id': 87654}
        mock_pipe = mock.Mock(spec=[pipeline_dict.keys()], **pipeline_dict)
        mock_gl_instance = fakes.FakeGitLab()
        mock_gl_instance.add_project(mock_pipe.ds_project_id, 'group/ds_project')
        ds_project = mock_gl_instance.projects.get(mock_pipe.ds_project_id)

        # No 'branch' in pipeline vars.
        ds_pipeline_vars = [{'key': 'what', 'value': 'huh'}]
        ds_project.add_pipeline(mock_pipe.ds_pipeline_id, variables=ds_pipeline_vars)
        self.assertIs(ckihook.get_downstream_pipeline_branch(mock_gl_instance, mock_pipe), None)

        # A 'branch' in pipeline vars.
        ds_pipeline_vars = [{'key': 'branch', 'value': '8.4'}]
        ds_project.add_pipeline(mock_pipe.ds_pipeline_id, variables=ds_pipeline_vars)
        self.assertEqual(ckihook.get_downstream_pipeline_branch(mock_gl_instance, mock_pipe), '8.4')

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_retry_pipelines(self):
        """Retries pipelines with a prepare_python job in their prepare stage."""
        mock_instance = fakes.FakeGitLab()
        mock_project = mock_instance.add_project(fake_payloads.PROJECT_ID,
                                                 fake_payloads.PROJECT_PATH_WITH_NAMESPACE)
        mock_pipeline = mock_project.add_pipeline(fake_payloads.PIPELINE_IID)
        mock_job = mock_project.add_job(123, 'prepare', 'success')

        job_gid = 'gid://Gitlab/job/123'
        pipe_data = {'ds_project_id': fake_payloads.PROJECT_ID,
                     'ds_pipeline_id': fake_payloads.PIPELINE_IID,
                     'get_stage': mock.Mock()}
        mock_pipe = mock.Mock(spec=list(pipe_data.keys()), **pipe_data)
        mock_pipe.get_stage.return_value = {'jobs': [{'id': job_gid, 'name': 'prepare_python'}]}
        ckihook.retry_pipelines(mock_instance, {1: mock_pipe})
        mock_job.retry.assert_called_once()
        mock_pipeline.retry.assert_called_once()

    def test_failed_rt_mrs(self):
        """Returns a dict with MR IDs as key and downstream pipeline nodes as values."""
        mock_graphql = mock.Mock()
        namespace = 'group/project'
        branches = ['9.1', '9.1-rt']
        mr1 = {'iid': 1, 'headPipeline': {'jobs': {'nodes': [1, 2, 3]}}}
        mr2 = {'iid': 2, 'headPipeline': {'jobs': {'nodes': [1, 2, 3]}}}
        mock_graphql.client.query.return_value = {'project': {'mrs': {'nodes': [mr1, mr2]}}}
        result = ckihook.failed_rt_mrs(mock_graphql, namespace, branches)
        self.assertEqual(result, {mr1['iid']: mr1['headPipeline']['jobs']['nodes'],
                                  mr2['iid']: mr2['headPipeline']['jobs']['nodes']})

    @mock.patch('webhook.ckihook.failed_rt_mrs')
    @mock.patch('webhook.ckihook.retry_pipelines')
    def test_retrigger_failed_pipelines(self, mock_retry, mock_failed_rt_mrs):
        """Finds failed pipelines and retries them."""
        mock_graphql = mock.Mock()
        mock_instance = mock.Mock()
        namespace = 'group/project'
        branch = mock.Mock(pipelines=[])
        branch.name = '9.0-rt'

        # No realtime pipeline for this branch, nothing to do.
        branch.pipelines = [PipelineType.RHEL]
        ckihook.retrigger_failed_pipelines(mock_graphql, mock_instance, namespace, branch)
        mock_failed_rt_mrs.assert_not_called()
        mock_retry.assert_not_called()

        # No MRs with failed on merge RT pipelines, nothing to do.
        branch.pipelines = [PipelineType.REALTIME]
        mock_failed_rt_mrs.return_value = {}
        ckihook.retrigger_failed_pipelines(mock_graphql, mock_instance, namespace, branch)
        mock_failed_rt_mrs.assert_called_once()
        mock_retry.assert_not_called()

        # Found some MRs, calls retry_pipelines.
        mr1_stages = {'nodes': [{'name': 'build', 'jobs': {'nodes': [{'status': 'FAILED'}]}},
                                {'name': 'test', 'jobs': {'nodes': [{'status': 'PENDING'}]}}]}
        mr2_stages = {'nodes': [{'name': 'merge', 'jobs': {'nodes': [{'status': 'FAILED'}]}},
                                {'name': 'test', 'jobs': {'nodes': [{'status': 'PENDING'}]}}]}
        mrs_data = {1: [{'name': 'c9s_rt_merge_request',
                         'downstreamPipeline': {'status': 'FAILED', 'stages': mr1_stages}}],
                    2: [{'name': 'c9s_rt_merge_request',
                         'downstreamPipeline': {'status': 'FAILED', 'stages': mr2_stages}}]}
        mock_failed_rt_mrs.return_value = mrs_data

        ckihook.retrigger_failed_pipelines(mock_graphql, mock_instance, namespace, branch)
        mock_retry.assert_called_once_with(mock_instance, mock.ANY)
        failed_pipes = mock_retry.call_args.args[1]
        self.assertEqual(list(failed_pipes.keys()), [2])
        self.assertEqual(failed_pipes[2].label, 'CKI_RT::Failed::merge')

    def test_pipeline_is_waived(self):
        """Returns True if the pipeline is waived, otherwise False."""
        mock_pipe = mock.Mock(type=PipelineType.REALTIME, status=ckihook.PipelineStatus.FAILED)
        self.assertTrue(ckihook.pipeline_is_waived(mock_pipe, ['CKI_RT::Waived']))
        mock_pipe = mock.Mock(type=PipelineType.RHEL, status=ckihook.PipelineStatus.FAILED)
        self.assertFalse(ckihook.pipeline_is_waived(mock_pipe, ['CKI::Waived']))
        mock_pipe = mock.Mock(type=PipelineType.AUTOMOTIVE, status=ckihook.PipelineStatus.FAILED)
        self.assertTrue(ckihook.pipeline_is_waived(mock_pipe, ['CKI_Automotive::Waived']))
        mock_pipe = mock.Mock(type=PipelineType.AUTOMOTIVE, status=ckihook.PipelineStatus.FAILED)
        self.assertFalse(ckihook.pipeline_is_waived(mock_pipe, ['CKI_RT::Waived']))

    @mock.patch('webhook.ckihook.generate_comment', mock.Mock())
    @mock.patch('webhook.common.update_webhook_comment', mock.Mock())
    def test_process_pipe_mr(self):
        """Checks the PipeMR's pipelines and updates the MR."""
        mock_pipe_mr = mock.Mock(pipelines=[], labels=['CKI_Automotive::Waived'],
                                 state=ckihook.MrState.OPENED)
        mock_pipe_mr.branch = mock.Mock(pipelines=[PipelineType.CENTOS, PipelineType.REALTIME,
                                                   PipelineType.AUTOMOTIVE])
        mock_pipe_mr.gl_project.protectedbranches.list.return_value = []
        # No pipelines, nothing to do.
        ckihook.process_pipe_mr(mock_pipe_mr)
        # A pipeline to process, RT missing.
        mock_pipe1 = mock.Mock(type=PipelineType.CENTOS, label='CKI_CentOS::OK',
                               status=ckihook.PipelineStatus.OK)
        mock_pipe2 = mock.Mock(type=PipelineType.AUTOMOTIVE, label='CKI_Automotive::Failed::merge',
                               status=ckihook.PipelineStatus.FAILED)
        mock_pipe_mr.pipelines = [mock_pipe1, mock_pipe2]
        ckihook.process_pipe_mr(mock_pipe_mr)
        mock_pipe_mr.add_labels.assert_called_with({'CKI::Missing',
                                                    'CKI_CentOS::OK',
                                                    'CKI_RT::Missing'},
                                                   remove_scoped=True)

    @mock.patch('webhook.ckihook.generate_comment', mock.Mock())
    @mock.patch('webhook.common.update_webhook_comment', mock.Mock())
    def test_process_pipe_mr_invalid_branch(self):
        """Checks invalid branch names are reported."""
        mock_pipe_mr = mock.Mock(pipelines=[], source_branch='9.0', labels=[],
                                 state=ckihook.MrState.OPENED)
        mock_pipe_mr.branch = mock.Mock(pipelines=[])
        mock_branch = mock.Mock()
        mock_branch.name = '9.*'
        mock_pipe_mr.gl_project.protectedbranches.list.return_value = [mock_branch]
        ckihook.process_pipe_mr(mock_pipe_mr)
        mock_pipe_mr.add_labels.assert_called_with({'CKI::Invalid'}, remove_scoped=True)

    def test_process_pipe_mr_not_opened(self):
        """Does nothing when the MR is not in 'opened' state."""
        mock_pipe_mr = mock.Mock(state=ckihook.MrState.CLOSED)
        with mock.patch('webhook.ckihook.update_mr') as mock_update_mr:
            ckihook.process_pipe_mr(mock_pipe_mr)
            mock_update_mr.assert_not_called()

    def test_generate_pipeline_labels(self):
        """Derives the set of CKI labels expected on the MR from the given data."""
        mock_pipe1 = mock.Mock(type=PipelineType.CENTOS, label='CKI_CentOS::OK',
                               status=ckihook.PipelineStatus.OK)
        mock_pipe2 = mock.Mock(type=PipelineType.AUTOMOTIVE, label='CKI_Automotive::Failed::merge',
                               status=ckihook.PipelineStatus.FAILED)
        mock_pipe3 = mock.Mock(type=PipelineType.RHEL_COMPAT, label='CKI_RHEL::OK',
                               status=ckihook.PipelineStatus.SUCCESS)
        mock_pipe4 = mock.Mock(type=PipelineType.REALTIME, label='CKI_RT::Running',
                               status=ckihook.PipelineStatus.RUNNING)
        pipe_dict = {PipelineType.RHEL: mock_pipe1,
                     PipelineType.AUTOMOTIVE: mock_pipe2,
                     PipelineType.RHEL_COMPAT: mock_pipe3,
                     PipelineType.REALTIME: mock_pipe4,
                     PipelineType._64K: None}
        mock_pipe_mr = mock.Mock(labels=['CKI_Automotive::Waived'])
        labels = ckihook.generate_pipeline_labels(mock_pipe_mr, pipe_dict)
        self.assertEqual(labels, {'CKI_CentOS::OK', 'CKI_64k::Missing', 'CKI_RT::Running',
                                  'CKI_RHEL::OK'})

    @mock.patch('webhook.common.update_webhook_comment')
    @mock.patch('webhook.ckihook.generate_comment')
    @mock.patch('webhook.ckihook.generate_pipeline_labels')
    @mock.patch('webhook.ckihook.generate_status_label')
    def test_update_mr(self, mock_gen_status_label, mock_gen_pipe_labels, mock_gen_comment,
                       mock_update_comment):
        """Updates labels and MR status comment as needed."""
        mock_gen_comment.return_value = 'Comment string'
        mock_pipelines = mock.Mock()

        # No labels to update
        mock_gen_status_label.return_value = 'CKI::OK'
        mock_gen_pipe_labels.return_value = {'CKI_RT::Failed::merge', 'CKI_64k::Missing'}
        mock_pipe_mr = mock.Mock(labels=['CKI::OK', 'Acks::OK', 'CKI_RT::Failed::merge',
                                         'CKI_64k::Missing'])
        ckihook.update_mr(mock_pipe_mr, mock_pipelines, None, [])
        mock_pipe_mr.add_labels.assert_not_called()

        # Labels to update.
        mock_gen_pipe_labels.return_value = {'CKI_RT::Failed::merge', 'CKI_64k::Missing'}
        mock_pipe_mr = mock.Mock(labels=['CKI_64k::Running'])
        ckihook.update_mr(mock_pipe_mr, mock_pipelines, None, [])
        mock_pipe_mr.add_labels.assert_called_once()


class TestCommentCode(TestCase):
    """Tests for the comment generating bits."""

    OK_PIPE = {'bridge_name': 'rhel10_merge_request',
               'ds_url': 'https://gitlab.com/g/p/-/pipelines/1',
               'label': 'CKI_RHEL::OK',
               'status': ckihook.PipelineStatus.OK,
               'type': PipelineType.RHEL}

    FAILED_PIPE = {'bridge_name': 'c9s_rt_merge_request',
                   'ds_url': 'https://gitlab.com/g/p/-/pipelines/1',
                   'label': 'CKI_RT::Failed::merge',
                   'status': ckihook.PipelineStatus.FAILED,
                   'type': PipelineType.REALTIME}

    CANCELED_PIPE = {'bridge_name': 'c9s_merge_request',
                     'ds_url': 'https://gitlab.com/g/p/-/pipelines/1',
                     'label': 'CKI_CentOS::Canceled',
                     'status': ckihook.PipelineStatus.CANCELED,
                     'type': PipelineType.CENTOS}

    RUNNING_PIPE = {'bridge_name': 'ark_merge_request',
                    'ds_url': 'https://gitlab.com/g/p/-/pipelines/1',
                    'label': 'CKI_ARK::Running',
                    'status': ckihook.PipelineStatus.RUNNING,
                    'type': PipelineType.ARK}

    def test_generate_comment_missing(self):
        """Returns a markdown string reporting all the pipeline results."""
        pipe_mr = mock.Mock(labels=[])
        pipe_mr.branch.pipelines = [PipelineType.RHEL, PipelineType.REALTIME]
        pipelines = {pipe: None for pipe in pipe_mr.branch.pipelines}
        comment_str = ckihook.generate_comment(pipe_mr, pipelines, 'CKI::Missing', None, [])
        self.assertIn('#### Missing pipelines', comment_str)
        self.assertIn('#### No blocking pipelines found.', comment_str)
        self.assertNotIn('#### Non-blocking pipelines', comment_str)

    def test_generate_comment_blocking(self):
        """Returns a markdown string reporting all pipeline results."""
        pipe_mr = mock.Mock(labels=[])
        pipe_mr.branch.pipelines = [PipelineType.ARK, PipelineType.REALTIME,
                                    PipelineType.AUTOMOTIVE, PipelineType._64K]
        pipe_list = [self.RUNNING_PIPE]
        pipelines = {pipe['type']: mock.Mock(**pipe) for pipe in pipe_list}
        for pipe in pipelines.values():
            pipe.allow_failure = False
        comment_str = ckihook.generate_comment(pipe_mr, pipelines, 'CKI::Running', None, [])
        self.assertIn('#### Blocking pipelines', comment_str)
        self.assertNotIn('#### Non-blocking pipelines', comment_str)

    def test_generate_comment_non_blocking(self):
        """Returns a markdown string reporting all pipeline results."""
        pipe_mr = mock.Mock(labels=[])
        pipe_mr.branch.pipelines = [PipelineType.ARK, PipelineType.REALTIME,
                                    PipelineType.AUTOMOTIVE, PipelineType._64K]
        pipe_list = [self.RUNNING_PIPE]
        pipelines = {pipe['type']: mock.Mock(**pipe) for pipe in pipe_list}
        for pipe in pipelines.values():
            pipe.allow_failure = True
        comment_str = ckihook.generate_comment(pipe_mr, pipelines, 'CKI::Running', None, [])
        self.assertIn('#### Non-blocking pipelines', comment_str)
        self.assertNotIn('#### Blocking pipelines', comment_str)


class TestMRHandler(TestCase):
    """Tests for the MR event handler."""

    @mock.patch('webhook.ckihook.get_pipe_mr')
    @mock.patch('webhook.ckihook.process_pipe_mr')
    def test_process_mr_event_label_changed(self, mock_process_pipe_mr, mock_get_pipe_mr):
        """Calls process_pipe_mr with the result of get_pipe_mr."""
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        payload['changes'] = deepcopy(CHANGES)
        payload['changes']['merge_status'] = 'pending'
        mock_session = mock.Mock()
        ckihook.process_mr_event(payload, mock_session)
        mock_process_pipe_mr.assert_called_once_with(mock_get_pipe_mr.return_value)

    @mock.patch('webhook.ckihook.get_pipe_mr')
    @mock.patch('webhook.ckihook.process_pipe_mr')
    def test_process_mr_event_no_label_change(self, mock_process_pipe_mr, mock_get_pipe_mr):
        """Returns without doing anything because the labels have not changed."""
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        payload['changes'] = {}
        mock_session = mock.Mock()
        ckihook.process_mr_event(payload, mock_session)
        mock_get_pipe_mr.assert_not_called()
        mock_process_pipe_mr.assert_not_called()


class TestPipelineHandler(TestCase):
    """Tests for the Pipeline event handler."""

    @mock.patch('webhook.ckihook.get_pipe_mr')
    @mock.patch('webhook.ckihook.process_pipe_mr')
    def test_process_pipeline_event_no_vars(self, mock_process_pipe_mr, mock_get_pipe_mr):
        """Doesn't do anything."""
        # No mr_url, nothing to do.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(fake_payloads.PIPELINE_PAYLOAD)
            mock_session = mock.Mock()
            ckihook.process_pipeline_event(payload, mock_session)
            self.assertIn('Event did not contain the expected variables', logs.output[-1])
            mock_get_pipe_mr.assert_not_called()
            mock_process_pipe_mr.assert_not_called()

        # Has mr_url, but retrigger is 'true'
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)
            payload['object_attributes']['variables'].append(
                {'key': 'CKI_DEPLOYMENT_ENVIRONMENT', 'value': 'retrigger'})
            mock_session = mock.Mock()
            ckihook.process_pipeline_event(payload, mock_session)
            self.assertIn('Event did not contain the expected variables', logs.output[-1])
            mock_get_pipe_mr.assert_not_called()
            mock_process_pipe_mr.assert_not_called()

    @mock.patch('webhook.ckihook.get_pipe_mr')
    @mock.patch('webhook.ckihook.process_pipe_mr')
    def test_process_pipeline_event_run_process_pipe(self, mock_process_pipe_mr, mock_get_pipe_mr):
        """Runs process_pipe_mr."""
        payload = deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)
        mock_session = mock.Mock()
        projects = mock.Mock()
        branch = projects.get_target_branch.return_value
        branch.pipelines = [PipelineType.RHEL]
        mock_session.rh_projects = projects
        ckihook.process_pipeline_event(payload, mock_session)
        mock_process_pipe_mr.assert_called_once_with(mock_get_pipe_mr.return_value)


class TestNoteHandler(TestCase):
    """Tests for the Note event handler."""

    @mock.patch('webhook.ckihook.get_pipe_mr')
    @mock.patch('webhook.ckihook.process_pipe_mr')
    def test_process_note_event(self, mock_process_pipe_mr, mock_get_pipe_mr):
        """Runs compute_new_labels and optionally add_labels if there is a proper request."""
        # No request, does nothing.
        payload = deepcopy(fake_payloads.NOTE_PAYLOAD)
        mock_session = mock.Mock()
        ckihook.process_note_event(payload, mock_session)
        mock_get_pipe_mr.assert_not_called()
        mock_process_pipe_mr.assert_not_called()

        # Evaluation request, calls process_pipe_mr.
        payload['object_attributes']['note'] = 'request-cki-evaluation'
        ckihook.process_note_event(payload, mock_session)
        mock_process_pipe_mr.assert_called_once_with(mock_get_pipe_mr.return_value)


class TestPushHandler(TestCase):
    """Tests for the Push event handler."""

    @mock.patch('webhook.ckihook.retrigger_failed_pipelines')
    def test_process_push_event(self, mock_retrigger):
        """Checks the target_branch for Rt and possibly retriggers pipelines."""
        body = deepcopy(fake_payloads.PUSH_PAYLOAD)
        mock_session = mock.Mock(rh_projects=mock.Mock())

        # Branch is not recognized, nothing to do.
        mock_session.rh_projects.get_target_branch.return_value = None
        ckihook.process_push_event(body, mock_session)
        mock_retrigger.assert_not_called()

        # Branch is not RT, nothing to do.
        mock_branch = mock.Mock()
        mock_branch.name = 'main'
        mock_session.rh_rojects.get_target_branch.return_value = mock_branch
        ckihook.process_push_event(body, mock_session)
        mock_retrigger.assert_not_called()

        # Branch is recognized and RT, away we go...
        mock_branch.name = '9.1-rt'
        mock_session.rh_projects.get_target_branch.return_value = mock_branch
        ckihook.process_push_event(body, mock_session)
        mock_retrigger.assert_called_once()
