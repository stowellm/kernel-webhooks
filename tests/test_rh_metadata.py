"""Webhook interaction tests."""
from unittest import TestCase
from unittest import mock

from webhook import rh_metadata
from webhook.defs import MrScope
from webhook.pipelines import PipelineType


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
class TestProjects(TestCase):
    """Tests for the Projects module."""

    def test_Projects(self):
        """Test Projects loading."""
        results = rh_metadata.Projects()

        self.assertEqual(len(results.projects), 3)
        self.assertEqual(len(results.projects[11223344].branches), 1)
        self.assertEqual(len(results.projects[12345].branches), 10)

        # THe number of hooks defined in the RH_METADATA_YAML_PATH file.
        self.assertEqual(len(results.webhooks), 14)

        self.assertIs(results.get_project_by_id(54321), None)
        self.assertIs(results.get_project_by_id(11223344), results.projects[11223344])
        self.assertEqual(results.get_project_by_id(11223344).product, 'Red Hat Enterprise Linux 9')
        self.assertEqual(results.get_project_by_id('gid://gitlab/Project/11223344').product,
                         'Red Hat Enterprise Linux 9')

        self.assertEqual(results.get_projects_by_name('rhel-10'), [])
        self.assertEqual(results.get_projects_by_name('rhel-8')[0].name, 'rhel-8')

        self.assertEqual(results.get_project_by_namespace('redhat/rhel/src/kernel/rhel-8').id,
                         12345)

    @mock.patch('webhook.rh_metadata.get_policy_data')
    def test_Projects_policies_loading(self, mock_get_policy_data):
        """Calls load_policies method."""
        mock_get_policy_data.return_value = {'c9s': [1, 2, 3],
                                             'rhel-8.7.0': None,
                                             'rhel-8.6.0': None,
                                             'rhel-8.5.0': [4, 5, 6]
                                             }
        results = rh_metadata.Projects(load_policies=True)
        self.assertEqual(results.projects[11223344].branches[0].policy, [1, 2, 3])
        self.assertEqual(results.projects[12345].branches[4].policy, [4, 5, 6])
        self.assertEqual(results.projects[12345].branches[5].policy, [4, 5, 6])
        for i in range(1, 4):
            self.assertEqual(results.projects[12345].branches[i].policy, [])
        for i in range(6, 10):
            self.assertEqual(results.projects[12345].branches[i].policy, [])


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
class TestProject(TestCase):
    """Tests for the Project module."""

    @mock.patch('webhook.rh_metadata.check_data', wraps=rh_metadata.check_data)
    def test_Project(self, mock_check_data):
        """Test Project objects."""
        results = rh_metadata.Projects()
        rh_metadata.check_data.assert_called()

        project = results.projects[12345]

        self.assertEqual(project.get_branch_by_name('1'), None)
        self.assertEqual(project.get_branch_by_name('main').distgit_ref, 'rhel-8.7.0')
        self.assertIs(project.get_branch_by_name('main'), project.branches[0])

        self.assertEqual(project.get_branches_by_itr('1.0'), [])
        self.assertEqual(project.get_branches_by_itr('8.7.0')[0].components, {'kernel'})
        self.assertEqual(project.get_branches_by_itr('8.7.0')[0].sub_component, '')
        self.assertEqual(project.get_branches_by_itr('8.7.0')[1].components, {'kernel-rt'})
        self.assertEqual(project.get_branches_by_itr('8.7.0')[1].sub_component, '')
        self.assertEqual(project.get_branches_by_itr('8.7.0')[2].components, {'kernel'})
        self.assertEqual(project.get_branches_by_itr('8.7.0')[2].sub_component, 'automotive')

        self.assertEqual(project.get_branches_by_ztr('10.1.0'), [])
        self.assertEqual(project.get_branches_by_ztr('8.5.0')[0].components, {'kernel'})
        self.assertEqual(project.get_branches_by_ztr('8.5.0')[0].sub_component, '')
        self.assertEqual(len(project.webhooks), 11)

        # branch is inactive so an empty list
        self.assertEqual(project.get_branches_by_ztr('8.1.0'), [])

        # Confirm Branches have a pointer back to the Project
        self.assertIs(project.branches[0].project, project)
        self.assertIs(project.branches[1].project, project)
        self.assertIs(project.branches[2].project, project)
        self.assertIs(project.branches[3].project, project)

    def test_webhooks_status(self):
        """Correctly returns the MrScope indicated by the labels."""
        rh_projects = rh_metadata.Projects()
        project = rh_projects.get_project_by_id(56789)
        all_ok_labels = [f'{prefix}::OK' for hook in project.webhooks.values() for
                         prefix in hook.label_prefixes]

        test_list = []
        # No labels is always NEEDS_REVIEW
        test_list.append((MrScope.NEEDS_REVIEW, []))
        # All OK labels, scope is OK
        test_list.append((MrScope.OK, all_ok_labels))
        # Bugzilla is NeedsTesting, overall is NEEDS_TESTING aka READY_FOR_QA
        qa_labels = [label for label in all_ok_labels if not label.startswith('Bugzilla::')]
        qa_labels.append('Bugzilla::NeedsTesting')
        test_list.append((MrScope.NEEDS_TESTING, qa_labels))
        test_list.append((MrScope.READY_FOR_QA, qa_labels))

        for scope, labels in test_list:
            with self.subTest(expected_scope=scope, labels=labels):
                self.assertIs(project.webhooks_status(labels), scope)


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
class TestBranch(TestCase):
    """Tests for the Branch module."""

    @mock.patch('webhook.rh_metadata.check_data', wraps=rh_metadata.check_data)
    def test_Branch(self, mock_check_data):
        """Test Branch objects."""
        webhooks = {'bughook': {'name': 'bughook'}}
        a_project = rh_metadata.Project(id=123, group_id=987, name='project', product='rhel',
                                        branches=[], pipelines=['rhel'], webhooks=webhooks)
        branch_input = {'name': 'test_branch',
                        'components': ['kernel-rt'],
                        'distgit_ref': 'rhel-2.5.5',
                        'internal_target_release': '2.5.5',
                        'pipelines': ['rhel', 'realtime'],
                        'project': a_project
                        }
        branch = rh_metadata.Branch(**branch_input)
        rh_metadata.check_data.assert_called()

        self.assertEqual(branch.name, 'test_branch')
        self.assertEqual(branch.components, {'kernel-rt'})
        self.assertEqual(branch.distgit_ref, 'rhel-2.5.5')
        self.assertEqual(branch.internal_target_release, '2.5.5')
        self.assertEqual(branch.zstream_target_release, '')
        self.assertEqual(branch.pipelines, [PipelineType.RHEL, PipelineType.REALTIME])
        self.assertEqual(branch.major, 2)
        self.assertEqual(branch.minor, 5)
        self.assertEqual(branch.stream, 5)
        self.assertEqual(branch.version, '2.5')
        self.assertIs(branch.is_alpha, False)
        self.assertIs(branch.is_beta, False)
        self.assertIs(branch.inactive, False)

    def test_branch_comparisons(self):
        """Returns the expected value for Branch comparisons."""
        results = rh_metadata.Projects()
        project1 = results.projects[11223344]
        project2 = results.projects[12345]

        # Same Branch, Same Project = Match.
        self.assertTrue(project1.branches[0] == project1.branches[0])
        self.assertFalse(project2.branches[0] != project2.branches[0])
        self.assertFalse(project2.branches[0] == project2.branches[1])

        # Branches from different Projects don't match.
        self.assertFalse(project1.branches[0] == project2.branches[1])

        # Comparison of Branches of different components raises a ValueError.
        # Comparison of Branches from different Projects raises a ValueError.
        with self.assertRaises(ValueError):
            project2.branches[0] > project2.branches[1]
            project1.branches[0] > project2.branches[1]

        # Higher version == greater than.
        self.assertTrue(project2.branches[0] > project2.branches[2] > project2.branches[4])
        self.assertTrue(project2.branches[1] > project2.branches[3] > project2.branches[5])

        # Lower version == less than.
        self.assertTrue(project2.branches[4] < project2.branches[2] < project2.branches[0])
        self.assertTrue(project2.branches[5] < project2.branches[3] < project2.branches[1])

        # Alpha < Beta < *
        self.assertTrue(project2.branches[7] < project2.branches[6] < project2.branches[4])


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
class TestHelpers(TestCase):
    """Tests for the helper methods."""

    def test_check_data(self):
        """Make sure the expected errors are raised."""
        webhooks = {'bughook': {'name': 'bughook'}}
        # Wrong field type
        project = rh_metadata.Project(id=123, group_id=987, name='project', product='rhel',
                                      webhooks=webhooks)
        branch = rh_metadata.Branch(name='test_branch', components=['kernel-rt'],
                                    distgit_ref='rhel-2.5.5', project=project)
        branch.__dict__['distgit_ref'] = 5
        err_raised = False
        try:
            rh_metadata.check_data(branch)
        except TypeError:
            err_raised = True
        self.assertTrue(err_raised)

        # Empty string for value with no default
        branch = rh_metadata.Branch(name='test_branch', components=['kernel-rt'],
                                    distgit_ref='rhel-2.5.5', project=project)
        branch.__dict__['components'] = set()
        with self.assertRaises(ValueError):
            rh_metadata.check_data(branch)

        # happy branch, empty default values don't raise a ValueError
        branch_input = {'name': 'test_branch',
                        'components': ['kernel-rt'],
                        'distgit_ref': 'rhel-2.5.5',
                        'internal_target_release': '2.5.5',
                        'project': project
                        }
        branch = rh_metadata.Branch(**branch_input)
        self.assertEqual(branch.internal_target_release, '2.5.5')
        self.assertEqual(branch.zstream_target_release, '')

    def test_is_branch_active(self):
        """Returns True if the branch exists and is not marked inactive in rh_metadata, or False."""
        projects = rh_metadata.Projects()
        self.assertTrue(rh_metadata.is_branch_active(projects, 11223344, 'main'))
        self.assertTrue(rh_metadata.is_branch_active(projects, 56789, 'main'))
        self.assertTrue(rh_metadata.is_branch_active(projects, 12345, 'main-rt'))
        self.assertFalse(rh_metadata.is_branch_active(projects, 12345, '8.1'))
        self.assertFalse(rh_metadata.is_branch_active(projects, 99999, 'main'))
        self.assertTrue(rh_metadata.is_branch_active(projects, '12345', '8.6-rt'))
