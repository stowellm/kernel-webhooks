"""Tests of the libjira library."""
from copy import deepcopy
from os import environ
from typing import Any
from typing import NamedTuple
from unittest import TestCase
from unittest import mock

from jira.exceptions import JIRAError
import responses

from tests import fakes_jira
from webhook import libjira
from webhook.defs import GITFORGE
from webhook.defs import JIStatus
from webhook.defs import JPFX
from webhook.defs import READY_FOR_QA_LABEL


class TestParseSearchList(TestCase):
    """Test the parse_search_list function."""

    class Test(NamedTuple):
        keys: list[str]
        cves: list[str]
        other: list[Any]
        raises_type: Exception | None

    def run_parse_search_list_test(self, test):
        """Runs the test."""
        search_list = test.keys + test.cves + test.other

        if test.raises_type:
            with self.assertRaises(test.raises_type):
                libjira.parse_search_list(search_list)
            return

        keys, cves = libjira.parse_search_list(search_list)
        self.assertCountEqual(test.keys, keys)
        self.assertCountEqual(test.cves, cves)

    def test_parse_search_list(self):
        """Test the parse_search_list function."""
        tests = [
            # Unexpected input, raises ValueError
            self.Test([], [], [1], ValueError),
            # Does what it is supposed to.
            self.Test(['RHEL-123'], ['CVE-2020-26526', 'CVE-2156-26262'], [], None),
            # Does what it is supposed to.
            self.Test([], ['CVE-2020-123456'], [], None),
            # Does what it is supposed to.
            self.Test(['RHEL-15623', 'RHEL-234'], ['CVE-1235-12341', 'CVE-1235-21351'], [], None),
            # Nothing to do.
            self.Test([], [], [], None),
        ]

        for count, test in enumerate(tests):
            with self.subTest(tests_index=count, test=test):
                self.run_parse_search_list_test(test)


class TestGetIssues(TestCase):
    """Test the _getissues function."""

    class Test(NamedTuple):
        keys: list[str]
        cves: list[str]
        raises_type: Exception | None
        raises_msg: str | None
        jira_error: JIRAError | None

    def run_getissues_test(self, test):
        """Run the test."""
        mock_jira = mock.Mock()
        if test.jira_error:
            mock_jira.search_issues.side_effect = test.jira_error

        if test.raises_type:
            with self.assertRaisesRegex(test.raises_type, test.raises_msg):
                libjira._getissues(mock_jira, issues=test.keys, cves=test.cves)
        else:
            result = libjira._getissues(mock_jira, issues=test.keys, cves=test.cves)

        if not test.keys or test.cves:
            mock_jira.assert_not_called()
            return

        jql_str = mock_jira.search_issues.call_args.args[0]
        for key in test.keys:
            self.assertIn(f'key={key}', jql_str)
        if test.cves:
            self.assertIn('labels in', jql_str)
        for cve in test.cves:
            self.assertIn(cve, jql_str)

        if not test.jira_error:
            self.assertEqual(result, mock_jira.search_issues.return_value)
        else:
            self.assertEqual(result, [])

    def test_getissues(self):
        """Test the _getissues function."""

        tests = [
            # No input, raises ValueError.
            self.Test([], [], ValueError, 'Search lists are empty.', False),
            # Queries an issue.
            self.Test(['RHEL-123'], [], None, None, False),
            # Queries a CVE.
            self.Test([], ['CVE-2020-123456'], None, None, False),
            # Queries a few issues and CVEs.
            self.Test(['RHEL-15623', 'RHEL-234'], ['CVE-1235-12341', 'CVE-1235-21351'],
                      None, None, None),
            # Queries an unknown issue, gets nothing back.
            self.Test(['RHEL-999999'], [], None, None, JIRAError)
        ]

        for count, test in enumerate(tests):
            with self.subTest(tests_index=count, test=test):
                self.run_getissues_test(test)


class TestHelpers(TestCase):
    """Tests for helper functions."""
    JIRA01 = fakes_jira.FakeJI(id=1, fields=mock.Mock(status='UNKNOWN'))
    JIRA02 = fakes_jira.FakeJI(id=2, fields=mock.Mock(status='NEW'))
    JIRA03 = fakes_jira.FakeJI(id=4, fields=mock.Mock(status='IN_PROGRESS'))
    JIRA04 = fakes_jira.FakeJI(id=5, fields=mock.Mock(status='READY_FOR_QA'))
    JIRA05 = fakes_jira.FakeJI(id=6, fields=mock.Mock(status='TESTED'))
    JIRA06 = fakes_jira.FakeJI(id=7, fields=mock.Mock(status='CLOSED'))

    def test_issues_with_lower_status(self):
        """Returns the JIRA Issues whose status is lower than status and at least min_status."""

        issue_list = [self.JIRA01, self.JIRA02, self.JIRA03, self.JIRA04,
                      self.JIRA05, self.JIRA06]
        self.assertCountEqual(libjira.issues_with_lower_status(issue_list, JIStatus.IN_PROGRESS),
                              [self.JIRA02])
        self.assertCountEqual(libjira.issues_with_lower_status(issue_list, JIStatus.TESTED),
                              [self.JIRA02, self.JIRA03, self.JIRA04])
        self.assertCountEqual(libjira.issues_with_lower_status(issue_list, JIStatus.CLOSED,
                              JIStatus.READY_FOR_QA), [self.JIRA04, self.JIRA05])

    def test_update_issue_status_no_prod(self):
        """Moves qualifying issues to the given status and returns them in a list."""
        # Given an empty issue_list there is nothing to do.
        self.assertEqual(libjira.update_issue_status([], JIStatus.IN_PROGRESS), [])

        # If none of the issues have a status less than the input, nothing to do.
        self.assertEqual(libjira.update_issue_status([self.JIRA05], JIStatus.IN_PROGRESS),
                         [])

        # Not production, just set the new status and return the list.
        issue1 = deepcopy(self.JIRA01)
        issue2 = deepcopy(self.JIRA02)
        issue3 = deepcopy(self.JIRA03)
        issue4 = deepcopy(self.JIRA04)
        issue_list = [issue1, issue2, issue3, issue4]
        result = libjira.update_issue_status(issue_list, JIStatus.READY_FOR_QA)
        self.assertEqual(result, [issue2, issue3])
        self.assertEqual(issue2.fields.status, 'NEW')
        self.assertEqual(issue3.fields.status, 'IN_PROGRESS')

    @mock.patch('webhook.libjira.is_production_or_staging', mock.Mock(return_value=True))
    @mock.patch('jira.JIRA.transition_issue')
    @mock.patch('jira.JIRA.add_comment')
    def test_update_issue_status_prod(self, add_comment, transition):
        """Moves qualifying issues to the given status and returns them in a list."""
        # Production, set the new status and return the list.
        issue1 = deepcopy(self.JIRA01)
        issue2 = deepcopy(self.JIRA02)
        issue3 = deepcopy(self.JIRA03)
        issue4 = deepcopy(self.JIRA04)
        issue5 = deepcopy(self.JIRA05)
        issue_list = [issue1, issue2, issue3, issue4, issue5]

        t_comment = "GitLab kernel MR bot updated status to IN_PROGRESS"
        result = libjira.update_issue_status(issue_list, JIStatus.IN_PROGRESS)
        self.assertEqual(transition.call_count, 1)
        status_name = JIStatus.IN_PROGRESS.name.replace('_', ' ')
        transition.assert_called_with(issue2, status_name, comment=t_comment)
        self.assertEqual(result, [issue2])
        self.assertEqual(issue1.fields.status, 'UNKNOWN')
        self.assertEqual(issue2.fields.status, 'IN_PROGRESS')
        self.assertEqual(issue3.fields.status, 'IN_PROGRESS')
        self.assertEqual(issue4.fields.status, 'READY_FOR_QA')
        self.assertEqual(issue5.fields.status, 'TESTED')

        transition.call_count = 0
        with self.assertLogs('cki.webhook.libjira', level='INFO') as logs:
            result = libjira.update_issue_status([issue3], JIStatus.READY_FOR_QA)
            transition.assert_not_called()
            self.assertEqual(result, [])
            self.assertIn("Setting Jira Issue's Preliminary Testing field to Requested",
                          logs.output[-1])

        # test NOT moving issue to TESTED
        with self.assertLogs('cki.webhook.libjira', level='INFO') as logs:
            issue4_status = issue4.fields.status
            result = libjira.update_issue_status([issue4], JIStatus.TESTED)
            self.assertEqual(result, [])
            self.assertIn('Unsupported transition status: TESTED', logs.output[-1])
            self.assertEqual(issue4.fields.status, issue4_status)

    @mock.patch('webhook.libjira.is_production_or_staging', mock.Mock(return_value=True))
    @mock.patch('webhook.libjira.issues_with_lower_status')
    def test_update_testable_builds(self, mock_ls):
        fields1 = mock.Mock(status=21, customfield_12321740='Contains pipeline_url in it')
        fields2 = mock.Mock(status=21, customfield_12321740='garbage')
        updater1 = mock.Mock()
        updater2 = mock.Mock()
        issue1 = mock.Mock(key=f'{JPFX}11223344', fields=fields1, update=updater1)
        issue2 = mock.Mock(key=f'{JPFX}22334455', fields=fields2, update=updater2)
        issue_list = [issue1, issue2]

        # No issue objects, nothing to return.
        mock_ls.return_value = []
        libjira.update_testable_builds(issue_list, 'text', ['pipeline_url'])
        issue1.update.assert_not_called()
        issue2.update.assert_not_called()

        # Comment already posted to one issue but not the other
        mock_ls.return_value = issue_list
        with self.assertLogs('cki.webhook.libjira', level='DEBUG') as logs:
            libjira.update_testable_builds(issue_list, 'text', ['pipeline_url'])
            self.assertIn(f'All downstream pipelines found in {JPFX}11223344', logs.output[-1])
            issue1.update.assert_not_called()
            issue2.update.assert_called_with(fields={'customfield_12321740': 'text'})

    def test_mark_issues_as_ready_for_testing(self):
        """Make sure we set Preliminary Testing: Requested when we need to."""
        labels = []
        issue_list = []
        mock_issue = mock.Mock()
        mock_issue.fields.customfield_12321540 = None
        mock_issue.key = 'RHEL-3'
        with self.assertLogs('cki.webhook.libjira', level='DEBUG') as logs:
            libjira.mark_issues_as_ready_for_testing(issue_list, labels)
            self.assertIn("Merge request not ready for QA", logs.output[-1])

            issue_list = [mock_issue]
            labels = [READY_FOR_QA_LABEL]
            libjira.mark_issues_as_ready_for_testing(issue_list, labels)
            self.assertIn("Setting Preliminary Testing field to Requested", logs.output[-1])

            mock_issue.fields.customfield_12321540 = mock.Mock(value='Requested')
            libjira.mark_issues_as_ready_for_testing(issue_list, labels)
            self.assertIn("Issue RHEL-3 already set to Requested", logs.output[-1])

    @mock.patch('webhook.libjira.is_production_or_staging', mock.Mock(return_value=True))
    @mock.patch('webhook.libjira.connect_jira')
    def test_add_gitlab_link_in_issues(self, mock_jira):
        """Test gitlab link additions in jira issues."""
        mr_id = 666
        url = f'{GITFORGE}/foo/bar/-/merge_requests/{mr_id}'
        icon_url = (f'{GITFORGE}/assets/favicon-'
                    '72a2cad5025aa931d6ea56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png')
        icon = {'url16x16': icon_url, 'title': 'GitLab Merge Request'}
        title = "A great MR"
        obj = mock.Mock(url=url, title=f'Merge Request: {title}', icon=icon)
        remote_link = mock.Mock(id=1112, object=obj)
        mock_jira().remote_link.return_value = remote_link
        mock_jira().remote_links.return_value = [remote_link]
        mock_MR = mock.Mock(mr_id=mr_id,
                            commits=True,
                            title=title,
                            description=mock.Mock(bugzilla=False),
                            namespace='foo/bar')
        mock_issue = mock.Mock(key=f'{JPFX}1234')
        with self.assertLogs('cki.webhook.libjira', level='INFO') as logs:
            libjira.add_gitlab_link_in_issues([mock_issue], mock_MR)
            self.assertIn(f'MR {mr_id} already linked in {JPFX}1234',
                          logs.output[-1])
            mock_jira().remote_links.return_value = []
            libjira.add_gitlab_link_in_issues([mock_issue], mock_MR)
            self.assertIn(f'Linking [Merge Request: A great MR]({url}) to issue {JPFX}1234',
                          logs.output[-1])
            exp_obj = {'url': url, 'title': f'Merge Request: {title}', 'icon': icon}
            mock_jira().add_simple_link.assert_called_with(issue=mock_issue, object=exp_obj)
            mock_jira().add_comment.assert_called_once()

    def test_remove_gitlab_link_comment_in_issue(self):
        """Test removal of comments we left in the issue pointing to our MR."""
        mock_issue = mock.Mock(id=55, key='RHEL-55')
        mock_author = mock.Mock()
        mock_author.name = 'gitlab-jira'
        mr_url = f'{GITFORGE}/foo/bar/-/merge_requests/66'
        mock_comment = mock.Mock(id=1234, body=f"Contains {mr_url} in it", author=mock_author)
        mock_jira = mock.Mock()
        mock_jira.comments.return_value = [mock_comment]
        mock_jira.comment.return_value = mock_comment
        with self.assertLogs('cki.webhook.libjira', level='INFO') as logs:
            libjira.remove_gitlab_link_comment_in_issue(mock_jira, mock_issue, mr_url)
            self.assertIn(f"Removing {mock_issue.key} comment pointing to {mr_url}",
                          logs.output[-1])
            mock_comment.delete.assert_called_once()

    @mock.patch('webhook.libjira._getissues')
    @mock.patch('webhook.libjira.connect_jira')
    def test_remove_gitlab_link_in_issues(self, mock_jira, mock_getissues):
        """Test gitlab link additions in jira issues."""
        mr_id = 666
        url = f'{GITFORGE}/foo/bar/-/merge_requests/{mr_id}'
        obj = mock.Mock(url=url, title='Merge request - blah blah',
                        icon={'url16x16': 'https://example.com/favicon.png',
                              'title': 'GitLab'})
        remote_link = mock.Mock(id=1112, object=obj)
        mock_jira().remote_link.return_value = remote_link
        mock_jira().remote_links.return_value = [remote_link]
        namespace = 'foo/bar'
        mock_getissues.return_value = [fakes_jira.JI7777777]

        # Ensure we do nothing if there's no issue list
        libjira.remove_gitlab_link_in_issues(mr_id, namespace, set())
        mock_getissues.assert_not_called()

        # make sure we call unlink functions when we do get an issue to remove
        with self.assertLogs('cki.webhook.libjira', level='INFO') as logs:
            libjira.remove_gitlab_link_in_issues(mr_id, namespace, [f'{JPFX}7777777'])
            self.assertIn(f'MR {mr_id} linked in {JPFX}7777777, removing it',
                          logs.output[-1])


MOCK_SERVER = 'https://issues.example.com'


@mock.patch.dict(environ, {'JIRA_SERVER': MOCK_SERVER})
class TestGetLinkedMrs(TestCase):
    """Tests for get_linked_mrs."""

    connect = {'baseUrl': MOCK_SERVER,
               'buildDate': '2023-01-01T00:00:00.000+0000',
               'buildNumber': 123456,
               'databaseBuildNumber': 123456,
               'deploymentType': 'Server',
               'scmInfo': '123',
               'serverTime': '2023-06-23T14:26:41.667+0000',
               'serverTitle': 'Issue Tracker',
               'version': '1.2.3',
               'versionNumbers': [1, 2, 3]}

    response1 = {'application': {},
                 'id': 7654321,
                 'object': {'icon': {'title': 'GitLab Merge Request',
                            'url16x16': 'https://gitlab.com/assets/favicon-123.png'},
                            'status': {'icon': {}},
                            'title': 'An awesome MR',
                            'url': 'https://gitlab.com/group/project/-/merge_requests/123'},
                 'self': f'{MOCK_SERVER}/rest/api/2/issue/JIRA-132/remotelink/7654321'}

    response2 = {'application': {},
                 'id': 2345678,
                 'object': {'icon': {'title': 'GitLab Merge Request',
                            'url16x16': 'https://gitlab.com/assets/favicon-123.png'},
                            'status': {'icon': {}},
                            'title': 'A weird MR',
                            'url': 'https://gitlab.com/coolgroup/coolproject/-/merge_requests/567'},
                 'self': f'{MOCK_SERVER}/rest/api/2/issue/JIRA-576/remotelink/2345678'}

    @mock.patch('webhook.libjira.connect_jira')
    def test_get_linked_mrs_exception(self, mock_connect_jira):
        """Catches JIRAError execptions and returns an empty list."""
        mock_connect_jira.remote_links.side_effect = JIRAError(text='busted')
        self.assertEqual(libjira.get_linked_mrs(''), [])

    @responses.activate
    def test_get_linked_mrs(self):
        """Returns the URL with the matching namespace."""
        responses.get(f'{MOCK_SERVER}/rest/api/2/serverInfo', json=self.connect)
        responses.get(f'{MOCK_SERVER}/rest/api/2/issue/JIRA-132/remotelink', json=[self.response1])
        responses.get(f'{MOCK_SERVER}/rest/api/2/issue/JIRA-576/remotelink', json=[self.response2])
        responses.get(f'{MOCK_SERVER}/rest/api/2/issue/JIRA-222/remotelink', json=[self.response1,
                                                                                   self.response2])
        tests = [{'jissue': 'JIRA-132',
                  'namespace': 'group/project',
                  'expected': [self.response1['object']['url']]},
                 {'jissue': 'JIRA-576',
                  'namespace': 'group/project',
                  'expected': []},
                 {'jissue': 'JIRA-576',
                  'namespace': 'coolgroup/coolproject',
                  'expected': [self.response2['object']['url']]},
                 {'jissue': 'JIRA-222',
                  'namespace': None,
                  'expected': [self.response1['object']['url'], self.response2['object']['url']]},
                 {'jissue': 'JIRA-222',
                  'namespace': 'group/project',
                  'expected': [self.response1['object']['url']]},
                 ]

        for test in tests:
            with self.subTest(**test):
                self.assertCountEqual(test['expected'],
                                      libjira.get_linked_mrs(test['jissue'], test['namespace']))
