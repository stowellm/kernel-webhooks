"""Tests for external CI module."""
import unittest
from unittest import mock

from tests import fakes
import webhook.common as common
import webhook.limited_ci as limited_ci
from webhook.session import SessionRunner


def init_fake_gitlab():
    """Initialize and return a fake gitlab.Gitlab instance."""
    glab = fakes.FakeGitLab()
    glab.add_group('group')
    glab.add_project(123, 'path/to/project')
    glab.add_project(456, 'outside-group')
    add_members_to = [glab.groups.get('group'),
                      glab.projects.get('path/to/project')]
    for g_or_p in add_members_to:
        g_or_p.members_all.add_member('testmember')
        g_or_p.members_all.add_member('anothermember')

    glab.user = fakes.FakeGitLabMember('good-bot')

    return glab


@mock.patch('webhook.common.is_event_target_branch_active', mock.Mock(return_value=True))
@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
class TestProcessMessage(unittest.TestCase):
    """Tests for common.process_message()."""

    HEADERS = {'message-type': 'gitlab'}

    def test_pipeline_call(self):
        """Verify correct handler is called for pipeline hook."""
        namespace = 'redhat/centos-stream/src/kernel/centos-stream-9'
        message = {'object_kind': 'pipeline',
                   'object_attributes': {
                       'id': 1,
                       'variables': [{'key': 'origin_path', 'value': 'none'}]
                   },
                   'commit': {'url': 'none'},
                   'user': {'username': 'testmember'},
                   'state': 'opened',
                   'project': {'web_url': 'project url',
                               'path_with_namespace': namespace}}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')
        test_session = SessionRunner('limited_ci', args, limited_ci.HANDLERS)
        test_session.gl_instance = fakes.FakeGitLab()
        test_session.gl_instance.auth()
        test_session.rh_projects = mock.Mock()

        with self.assertLogs('cki.webhook.limited_ci', level='INFO') as logs:
            test_session.process_one_message(routing_key='key', body=message, headers=self.HEADERS)
            self.assertIn('No matching MR project for none', logs.output[-1])

    def test_mr_hook_no_project(self):
        """Verify MR with unknown project is handled correctly."""
        namespace = 'redhat/centos-stream/src/kernel/centos-stream-9'
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': namespace},
                   'user': {'username': 'testmember'},
                   'state': 'opened'}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')
        test_session = SessionRunner('limited_ci', args, limited_ci.HANDLERS)
        test_session.gl_instance = fakes.FakeGitLab()
        test_session.gl_instance.auth()
        test_session.rh_projects = mock.Mock()

        with self.assertLogs('cki.webhook.limited_ci', level='ERROR') as logs:
            test_session.process_one_message(routing_key='key', body=message, headers=self.HEADERS)
            self.assertIn('Missing config for', logs.output[-1])

    @mock.patch('webhook.limited_ci.get_project_config')
    def test_mr_hook_group_member(self, mock_config):
        """Check pipelines aren't triggered if the author is a group member."""
        namespace = 'redhat/centos-stream/src/kernel/centos-stream-9'
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': namespace},
                   'user': {'username': 'testmember'},
                   'state': 'opened'}
        mock_config.return_value = {'.members_of': 'group'}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')
        test_session = SessionRunner('limited_ci', args, limited_ci.HANDLERS)
        test_session.gl_instance = init_fake_gitlab()
        test_session.rh_projects = mock.Mock()

        with self.assertLogs('cki.webhook.limited_ci', level='DEBUG') as logs:
            test_session.process_one_message(routing_key='key', body=message, headers=self.HEADERS)
            self.assertIn('Found internal contributor', logs.output[-1])

    @mock.patch('webhook.limited_ci.get_project_config')
    def test_mr_hook_project_member(self, mock_config):
        """Test fallback with testing for a project member instead of group."""
        namespace = 'redhat/centos-stream/src/kernel/centos-stream-9'
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': namespace},
                   'user': {'username': 'anothermember'},
                   'state': 'opened'}
        mock_config.return_value = {'.members_of': 'path/to/project'}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')
        test_session = SessionRunner('limited_ci', args, limited_ci.HANDLERS)
        test_session.gl_instance = init_fake_gitlab()
        test_session.rh_projects = mock.Mock()

        with self.assertLogs('cki.webhook.limited_ci', level='DEBUG') as logs:
            test_session.process_one_message(routing_key='key', body=message, headers=self.HEADERS)
            self.assertIn('Found internal contributor', logs.output[-1])

    @mock.patch('webhook.limited_ci.get_project_config')
    def test_mr_hook_no_code_changes(self, mock_config):
        """Check we abort if the MR code was not modified."""
        namespace = 'redhat/centos-stream/src/kernel/centos-stream-9'
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': namespace},
                   'user': {'username': 'testmember'},
                   'state': 'opened',
                   'object_attributes': {'action': 'not-a-code-change'}}
        mock_config.return_value = {'.members_of': 'outside-group'}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')
        test_session = SessionRunner('limited_ci', args, limited_ci.HANDLERS)
        test_session.gl_instance = init_fake_gitlab()
        test_session.rh_projects = mock.Mock()

        with self.assertLogs('cki.webhook.limited_ci', level='DEBUG') as logs:
            test_session.process_one_message(routing_key='key', body=message, headers=self.HEADERS)
            self.assertIn('Not a code change, ignoring', logs.output[-1])


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
class TestMain(unittest.TestCase):
    """Sanity tests for limited_ci.main()."""

    @mock.patch.dict('os.environ', {'CONFIG_PATH': 'filepath'})
    @mock.patch.dict('webhook.limited_ci.CI_CONFIG',
                     {'test': {'.mr_project': 'path/to/project',
                               '.members_of': 'path/to/project'}})
    @mock.patch('webhook.limited_ci.load_config', mock.Mock())
    @mock.patch('webhook.session.get_instance')
    def test_manual(self, mock_gitlab):
        """Verify manual request is correctly handled.

        This includes checking get_manual_hook_data() function.
        """
        args = ['--merge-request',
                'https://gitlab.example/path/to/project/-/merge_requests/11']
        fake_gitlab = init_fake_gitlab()
        fake_project = fake_gitlab.projects.get('path/to/project')
        fake_project.attributes = {
            'name': 'path/to/project',
            'http_url_to_repo': 'https://gitlab.example/path/to/project'
        }
        fake_project.add_mr(
            11,
            {'sha': 'last_commit',
             'author': {'username': 'testmember'},
             'target_branch': 'devel'},
            {'web_url': 'https://url'}
        )
        mock_gitlab.return_value = fake_gitlab

        with self.assertLogs('cki.webhook.limited_ci', level='DEBUG') as logs:
            limited_ci.main(args)
            self.assertIn('Found internal contributor', logs.output[-1])


class TestStageSuffix(unittest.TestCase):
    """Tests for limited_ci.get_stage_suffix()."""

    def test_suffix(self):
        """Verify the function returns expected strings."""
        jobs = [fakes.FakeGitLabJob(123, 'stage', 'success')]
        self.assertEqual('', limited_ci.get_stage_suffix(jobs))

        jobs = [fakes.FakeGitLabJob(123, 'stage', 'success'),
                fakes.FakeGitLabJob(125, 'stage2', 'skipped')]
        self.assertEqual('', limited_ci.get_stage_suffix(jobs))

        jobs = [fakes.FakeGitLabJob(123, 'stage', 'success'),
                fakes.FakeGitLabJob(125, 'stage2', 'failed')]
        self.assertEqual('::stage2', limited_ci.get_stage_suffix(jobs))

        jobs = [fakes.FakeGitLabJob(123, 'stage', 'failed'),
                fakes.FakeGitLabJob(125, 'stage2', 'success')]
        self.assertEqual('::stage', limited_ci.get_stage_suffix(jobs))


@mock.patch('webhook.common.is_event_target_branch_active', mock.Mock(return_value=True))
@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
class TestUpdatePipelineData(unittest.TestCase):
    """Tests for limited_ci.update_pipeline_data()."""

    HEADERS = {'message-type': 'gitlab'}

    @mock.patch.dict('webhook.limited_ci.CI_CONFIG',
                     {'test': {'.mr_project': 'path/to/project',
                               '.members_of': 'path/to/project'}})
    def test_no_project(self):
        """Verify correct behavior when no matching MR project is found."""
        namespace = 'redhat/centos-stream/src/kernel/centos-stream-9'
        message = {'object_kind': 'pipeline',
                   'object_attributes': {'id': 10, 'variables': []},
                   'user': {'username': 'testmember'},
                   'commit': {'url': 'link'},
                   'project': {'web_url': 'project url',
                               'path_with_namespace': namespace}}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')
        test_session = SessionRunner('limited_ci', args, limited_ci.HANDLERS)
        test_session.gl_instance = fakes.FakeGitLab()
        test_session.gl_instance.auth()
        test_session.rh_projects = mock.Mock()

        with self.assertLogs('cki.webhook.limited_ci', level='INFO') as logs:
            test_session.process_one_message(routing_key='key', body=message, headers=self.HEADERS)
            self.assertIn('No matching MR project', logs.output[-1])

    @mock.patch.dict('webhook.limited_ci.CI_CONFIG',
                     {'test': {'.mr_project': 'path/to/project',
                               '.members_of': 'path/to/project'}})
    def test_retriggered(self):
        """Verify correct behavior when the pipeline is a retrigger."""
        namespace = 'redhat/centos-stream/src/kernel/centos-stream-9'
        message = {'object_kind': 'pipeline',
                   'object_attributes': {'id': 10, 'status': 'failed', 'variables': [
                       {'key': 'CKI_DEPLOYMENT_ENVIRONMENT', 'value': 'retrigger'},
                       {'key': 'origin_path', 'value': 'path/to/project'}
                   ]},
                   'user': {'username': 'testmember'},
                   'commit': {'url': 'link'},
                   'project': {'web_url': 'project url',
                               'path_with_namespace': namespace}}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')
        test_session = SessionRunner('limited_ci', args, limited_ci.HANDLERS)
        test_session.gl_instance = init_fake_gitlab()
        test_session.rh_projects = mock.Mock()

        with self.assertLogs('cki.webhook.limited_ci', level='INFO') as logs:
            test_session.process_one_message(routing_key='key', body=message, headers=self.HEADERS)
            self.assertIn('Handling a retriggered pipeline', logs.output[-2])


class TestHandleMR(unittest.TestCase):
    """Tests for limited_ci.handle_mr()."""

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.limited_ci.already_commented')
    def test_no_config(self, mock_commented, mock_labeler):
        """Verify the function bails out if there is no pipeline project/branch config."""
        mr_data = {
            'object_attributes': {
                'iid': 11,
                'target_branch': 'target',
                'url': 'https://dummy-link-to.mr',
                'last_commit': {
                    'id': '0123abcd'
                },
                'target': {
                    'git_http_url': 'giturl'
                }
            },
            'project': {
                'path_with_namespace': 'path/to/project',
                'id': 123
            }
        }
        project_config = {
            '.mr_project': 'path/to/project',
            '.members_of': 'path/to/project'
        }
        glab = init_fake_gitlab()
        mock_mr = glab.projects.get('path/to/project').add_mr(11)
        mock_mr.diff_refs = {'base_sha': 'blablabla'}
        mock_commented.return_value = True

        with self.assertLogs('cki.webhook.limited_ci', level='INFO') as logs:
            limited_ci.handle_mr(glab, project_config, mr_data)
            self.assertIn('No pipeline project or branch configured', logs.output[-1])

    @mock.patch('webhook.limited_ci.cki_pipeline.trigger_multiple')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.limited_ci.already_commented')
    def test_handle_mr(self, mock_commented, mock_label, mock_trigger):
        """Verify the function doesn't do anything weird."""
        mr_data = {
            'object_attributes': {
                'iid': 11,
                'target_branch': 'target',
                'url': 'https://dummy-link-to.mr',
                'last_commit': {
                    'id': '0123abcd'
                },
                'target': {
                    'git_http_url': 'giturl'
                }
            },
            'project': {
                'path_with_namespace': 'path/to/project',
                'id': 123
            }
        }
        project_config = {
            '.mr_project': 'path/to/project',
            '.members_of': 'path/to/project',
            '.cki_external_pipeline_branch': 'branch',
            '.cki_external_pipeline_project': 'project'
        }
        glab = init_fake_gitlab()
        mock_mr = glab.projects.get('path/to/project').add_mr(11)
        mock_mr.diff_refs = {'base_sha': 'blablabla'}
        mock_commented.return_value = True

        mock_trigger.return_value = [fakes.FakeGitLabPipeline(123, {'web_url': 'https://url'})]

        with self.assertLogs('cki.webhook.limited_ci', level='INFO') as logs:
            limited_ci.handle_mr(glab, project_config, mr_data)
            self.assertIn('STATUS COMMENT', logs.output[-1])

    @mock.patch('webhook.limited_ci.sleep', mock.Mock())
    def test_wait_for_attribute(self):
        """Loops until the instance attribute exists and is not null."""
        mr1 = mock.Mock(spec=[])
        mr2 = mock.Mock(diff_refs=None)
        mr3 = mock.Mock(diff_refs={'hi': 'there'})
        mock_project = mock.Mock()
        mock_project.mergerequests.get.side_effect = [mr1, mr2, mr3]
        kwargs = {'id': 123}
        with self.assertLogs('cki.webhook.limited_ci', level='INFO') as logs:
            result = limited_ci.wait_for_attribute(mr1, 'diff_refs',
                                                   mock_project.mergerequests.get, kwargs)
            self.assertIn('diff_refs after 3 loops', logs.output[-1])
            self.assertIs(result, mr3)
            self.assertEqual(mock_project.mergerequests.get.call_count, 3)
            mock_project.mergerequests.get.assert_called_with(**kwargs)

        # Raises ValueError if we loop too much.
        mock_project.mergerequests.get.reset_mock(side_effect=True)
        mock_project.mergerequests.get.return_value = mr1
        with self.assertRaises(ValueError):
            limited_ci.wait_for_attribute(mr1, 'diff_refs',
                                          mock_project.mergerequests.get, {'id': 123})
            self.assertEqual(mock_project.mergerequests.get.call_count, 5)
