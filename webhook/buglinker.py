"""Update bugs to contain links to an MR and successful pipeline artifacts."""
from os import environ
import re
import sys
from typing import TYPE_CHECKING
from xmlrpc.client import Fault

from bugzilla import BugzillaError
from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_variables
from gitlab.v4.objects.pipelines import ProjectPipelineBridge

from . import common
from . import defs
from .description import Description
from .jissue import make_jissues
from .libjira import remove_gitlab_link_in_issues
from .libjira import update_testable_builds
from .pipelines import BridgeJob
from .pipelines import PipelineType
from .session import new_session

if TYPE_CHECKING:
    from gitlab.v4.objects.merge_requests import ProjectMergeRequest
    from gitlab.v4.objects.projects import Project

    from webhook.rh_metadata import Branch

LOGGER = logger.get_logger('cki.webhook.buglinker')

HEADER = ('The following Merge Request has pipeline job artifacts available:\n\n'
          'Title: %s\nMR: %s\n'
          'MR Pipeline: %s\n\n'
          'The Repo URLs are *not* accessible from a web browser! '
          'They only function as a dnf or yum baseurl.\n\n'
          'Artifacts expire six weeks after creation. If artifacts are needed after that '
          "time please rerun the pipeline by visiting the MR's 'Pipelines' tab and clicking "
          "the 'Run pipeline' button.\n\n")


def get_bugs(bzcon, bug_list):
    """Return a list of bug objects."""
    try:
        bz_results = bzcon.getbugs(bug_list)
        if not bz_results:
            LOGGER.info("getbugs() returned an empty list for these bugs: %s.", bug_list)
        return bz_results
    except BugzillaError:
        LOGGER.exception('Error getting bugs.')
        return None


def make_ext_bz_bug_id(mr_url):
    """Format the string needed for the tracker links."""
    namespace, mr_id = common.parse_mr_url(mr_url)
    return f"{namespace}/-/merge_requests/{mr_id}"


def update_bugzilla(bugs, mr_url, bzcon):
    """Filter input bug list and run bugzilla API actions."""
    ext_bz_bug_id = make_ext_bz_bug_id(mr_url)
    for action, bug_list in bugs.items():
        if not bug_list or action not in ('link', 'unlink'):
            continue
        if action == 'unlink':
            unlink_mr_from_bzs(bug_list, ext_bz_bug_id, bzcon)
        else:
            bugs = get_bugs(bzcon, bug_list)
            link_mr_to_bzs(bugs, ext_bz_bug_id, bzcon)


def bz_is_linked_to_mr(bug, ext_bz_bug_id):
    """Return the matching external tracker for the BZ, if any."""
    return next((tracker for tracker in bug.external_bugs if
                 tracker['type']['description'] == 'Gitlab' and
                 tracker['type']['url'] == defs.EXT_TYPE_URL and
                 tracker['ext_bz_bug_id'] == ext_bz_bug_id),
                None)


def unlink_mr_from_bzs(bugs, ext_bz_bug_id, bzcon):
    """Unlink the MR from the given list of BZs."""
    # Add any related kernel-rt bugs
    rt_bugs = {bug.id for bug in get_rt_cve_bugs(bzcon, bugs)}
    for bug in set(bugs).union(rt_bugs):
        LOGGER.info('Unlinking %s from BZ%s.', ext_bz_bug_id, bug)
        if not misc.is_production_or_staging():
            continue
        try:
            bzcon.remove_external_tracker(ext_type_description='Gitlab',
                                          ext_bz_bug_id=ext_bz_bug_id, bug_ids=bug)
        except BugzillaError:
            LOGGER.exception("Problem unlinking %s from BZ%s.", ext_bz_bug_id, bug)
        except Fault as err:
            if err.faultCode == 1006:
                LOGGER.warning('xmlrpc fault %d: %s', err.faultCode, err.faultString)
            else:
                raise


def get_kernel_bugs(bugs):
    """Filter out bugs which are not RHEL & a kernel component."""
    return [bug for bug in bugs if
            bug.product.startswith('Red Hat Enterprise Linux') and
            bug.component in ('kernel', 'kernel-rt')]


def _do_link_mr_to_bzs(bugs, ext_bz_bug_id, bzcon):
    if misc.is_production_or_staging():
        try:
            bzcon.add_external_tracker(bugs, ext_type_url=defs.EXT_TYPE_URL,
                                       ext_bz_bug_id=ext_bz_bug_id)
        except BugzillaError:
            LOGGER.exception("Problem adding tracker %s to BZs.", ext_bz_bug_id)


def link_mr_to_bzs(bugs, ext_bz_bug_id, bzcon):
    """Take a list of bugs and a MR# and update the BZ's ET with the MR url."""
    untracked_bugs = [bug for bug in bugs if not bz_is_linked_to_mr(bug, ext_bz_bug_id)]
    if not untracked_bugs:
        LOGGER.info("All bugs have an existing link to %s.", ext_bz_bug_id)
        return

    kernel_bugs = get_kernel_bugs(untracked_bugs)
    bug_list = [bug.id for bug in kernel_bugs]

    LOGGER.info("Need to add %s to external tracker list of these bugs: %s", ext_bz_bug_id,
                bug_list)
    _do_link_mr_to_bzs(bug_list, ext_bz_bug_id, bzcon)


BASIC_BZ_FIELDS = ['component',
                   'external_bugs',
                   'id',
                   'product',
                   'sub_component'
                   'summary'
                   ]

BASIC_BZ_QUERY = {'query_format': 'advanced',
                  'include_fields': BASIC_BZ_FIELDS,
                  'classification': 'Red Hat',
                  'component': 'kernel-rt'
                  }


def parse_cve_from_summary(summary):
    """Return a CVE ID from the string, or None."""
    result = re.match(r'^(\w* )?(?P<cve>CVE-\d{4}-\d{4,7})\s', summary)
    if result:
        return result.group('cve')
    return None


def get_rt_cve_bugs(bzcon, bug_list):
    """Identify whether bug_list BZs are CVEs and if so return matching kernel-rt bugs."""
    # Filter out any kernel-rt bugs given in the bug_list.
    bz_results = [bug for bug in get_bugs(bzcon, bug_list) if bug.component != 'kernel-rt']
    cve_set = set()
    for bug in bz_results:
        cve_id = parse_cve_from_summary(bug.summary)
        if not cve_id:
            continue
        cve_set.add(cve_id)

    if not cve_set:
        return []

    cve_query = {**BASIC_BZ_QUERY}
    cve_query['product'] = bz_results[0].product
    # Blocked by the CVE
    cve_query['f1'] = 'blocked'
    cve_query['o1'] = 'anywords'
    cve_query['v1'] = list(cve_set)
    # Is not CLOSED DUPLICATE.
    cve_query['f2'] = 'resolution'
    cve_query['o2'] = 'notequals'
    cve_query['v2'] = 'DUPLICATE'
    # Is the same version.
    cve_query['f3'] = 'version'
    cve_query['o3'] = 'equals'
    cve_query['v3'] = bz_results[0].version

    try:
        return bzcon.query(cve_query)
    except BugzillaError:
        LOGGER.exception('Error querying bugzilla.')
        return []


def post_to_bugs(bug_list, header, bridge_job, bzcon):
    # pylint: disable=too-many-branches
    """Submit post to given bugs."""
    LOGGER.info('Posting results for %s. bug_list is: %s', bridge_job.type.name, bug_list)
    filtered_bugs = []
    if not (bugs := get_bugs(bzcon, bug_list)):
        LOGGER.warning('No bug data found.')
        return None
    # If this is the RT pipeline and a CVE then we want to post to the kernel-rt bug.
    if bridge_job.type is PipelineType.REALTIME:
        if rt_bugs := get_rt_cve_bugs(bzcon, bug_list):
            LOGGER.info('Including CVE bugs %s', rt_bugs)
            bugs.extend(rt_bugs)
        else:
            LOGGER.info('No RT CVE bug data found.')

    # If this is an automotive pipeline then only post if the BZs' component is kernel-automotive.
    if bridge_job.type is PipelineType.AUTOMOTIVE:
        if not (bugs := [bug for bug in bugs if bug.component == 'kernel-automotive']):
            LOGGER.info("Ignoring automotive pipeline as no bug has 'automotive' subcomponent")
            return None

    # It is possible for there to be multiple 'success' events for a given pipeline
    # so we should check that we haven't already posted them to the bugs. Sigh.
    for bug in bugs:
        if bug.id not in filtered_bugs and not \
           comment_already_posted(bug, bridge_job.ds_pipeline.web_url):
            filtered_bugs.append(bug.id)
    if not filtered_bugs:
        LOGGER.info('This pipeline has already been posted to all relevant bugs.')
        return None

    post_text = header + bridge_job.artifacts_text
    LOGGER.info('Updating these bugs %s with comment:\n%s', filtered_bugs, post_text)
    if misc.is_production_or_staging():
        # Adding a comment with extra_private_groups is not possible with
        # python-bugzilla (it misses support for add_comment, you can only
        # update existing comments). Thus call the api directly using
        # JSON RPC.
        data = {
            "method": "Bug.add_comment",
            "version": "2.0",
            "params": {
                "id": 0,
                "comment": post_text,
                "is_private": 1,
                "minor_update": 0,
                "extra_private_groups": ["redhat_partner_engineers"]
            },
        }
        bz_url = bzcon.url.replace('xmlrpc.cgi', 'jsonrpc.cgi')
        sess = bzcon.get_requests_session()
        for bug in filtered_bugs:
            data["params"]["id"] = bug
            res = sess.post(url=bz_url, json=data,
                            headers={'Content-Type': 'application/json'})
            res.raise_for_status()
            rdict = res.json()
            LOGGER.debug('Added comment %s to bug %s', rdict["result"]["id"], bug)
    return bugs


def comment_already_posted(bug, pipeline_url):
    """Return True if the pipeline results have already been posted to the given bug."""
    pipeline_text = f'Downstream Pipeline: {pipeline_url}'
    comments = bug.getcomments()
    for comment in comments:
        if comment['creator'] == environ['BUGZILLA_EMAIL'] and pipeline_text in comment['text']:
            LOGGER.info('Excluding bug %s as pipeline was already posted in comment %s.', bug.id,
                        comment['count'])
            return True
    return False


def check_associated_mr(merge_request):
    """Return head pipeline ID or None based on whether we care about the MR."""
    if merge_request.work_in_progress:
        LOGGER.info("MR %s is marked work in progress, ignoring.", merge_request.iid)
        return None
    if merge_request.head_pipeline is None:
        LOGGER.info("MR %s has not triggered any pipelines? head_pipeline is None.",
                    merge_request.iid)
        return None

    pipeline_id = merge_request.head_pipeline.get('id')
    if not merge_request.head_pipeline.get('web_url', '').startswith(f'{defs.GITFORGE}/redhat/'):
        LOGGER.info("MR %s head pipeline #%s is not in the Red Hat namespace: %s",
                    merge_request.iid, pipeline_id, merge_request.head_pipeline.get('web_url'))
        return None

    return pipeline_id


def filter_bridge_jobs(
        jobs: dict[PipelineType, BridgeJob],
        branch: 'Branch'
        ) -> dict[PipelineType, BridgeJob]:
    """Return a filtered jobs dict without pipelines not expected on the Branch, etc."""
    filtered_jobs = {}
    for bridge_job in jobs.values():
        if bridge_job.type not in branch.pipelines:
            LOGGER.info("Ignoring job because it doesn't get an MR label on branch '%s': %s",
                        branch.name, bridge_job)
            continue
        if not bridge_job.builds_valid:
            LOGGER.info("Ignoring job because it doesn't have valid builds: %s", bridge_job)
            continue
        filtered_jobs[bridge_job.type] = bridge_job
    # If this Branch has a compat pipeline then we want to avoid posting centos pipeline results
    # but then also only post the compat pipeline results if the centos pipeline has finished. Ha.
    if filtered_jobs and PipelineType.RHEL_COMPAT in branch.pipelines:
        if PipelineType.CENTOS not in filtered_jobs:
            if popped_compat := filtered_jobs.pop(PipelineType.RHEL_COMPAT, None):
                LOGGER.info('Ignoring job as there are no centos pipeline results at this time: %s',
                            popped_compat)
        if popped_centos := filtered_jobs.pop(PipelineType.CENTOS, None):
            LOGGER.info('Ignoring job for a Branch with rhel compat: %s', popped_centos)
    return filtered_jobs


def fetch_bridge_jobs(
        bridge_jobs_list: list[ProjectPipelineBridge]
        ) -> dict[PipelineType, BridgeJob]:
    """Return a dict of BridgeJobArtifacts."""
    jobs_dict = {}
    for bridge_job in bridge_jobs_list:
        bridge_job_data = BridgeJob(bridge_job)
        if bridge_job_data.type in jobs_dict:
            raise RuntimeError('Multiple bridge jobs have the same PipelineType!')
        jobs_dict[bridge_job_data.type] = bridge_job_data
    return jobs_dict


def set_targeted_testing_label(
        gl_project: 'Project',
        gl_mr: 'ProjectMergeRequest',
        needs_missing_tests_label: bool
        ) -> None:
    """Add or remove the targeted testing label as needed.."""
    has_missing_tests_label = defs.TARGETED_TESTING_LABEL in gl_mr.labels
    LOGGER.info('%s label needed: %s, has: %s', defs.TARGETED_TESTING_LABEL,
                needs_missing_tests_label, has_missing_tests_label)
    if needs_missing_tests_label and not has_missing_tests_label:
        LOGGER.info('Adding %s label.', defs.TARGETED_TESTING_LABEL)
        common.add_label_to_merge_request(gl_project, gl_mr.iid, [defs.TARGETED_TESTING_LABEL])
    if not needs_missing_tests_label and has_missing_tests_label:
        LOGGER.info('Removing %s label.', defs.TARGETED_TESTING_LABEL)
        common.remove_labels_from_merge_request(gl_project, gl_mr.iid,
                                                [defs.TARGETED_TESTING_LABEL])


def post_mr_pipelines(
        bridge_jobs: list[BridgeJob],
        bz_ids: set[int],
        jissues: set[int],
        mr_url: str,
        header_str: str
        ) -> None:
    """Post artifact data to the given bz_ids and jissues."""
    # The header and full comment for our posts.
    comments = {job.type: job.artifacts_text for job in bridge_jobs}
    comment_text = header_str + '\n'.join(dict(sorted(comments.items())).values())
    LOGGER.info('Full comment text:\n%s', comment_text)

    # Post a comment per-pipeline to each BZ.
    if bz_ids:
        if not (bzcon := common.try_bugzilla_conn()):
            raise RuntimeError('No bugzilla connection!')
        ext_bz_bug_id = make_ext_bz_bug_id(mr_url)
        for bridge_job in bridge_jobs:
            bugs = post_to_bugs(bz_ids, header_str, bridge_job, bzcon)
            # For kernel-rt we separately add external tracker links to those CVE BZs.
            if bridge_job.type is PipelineType.REALTIME and bugs:
                link_mr_to_bzs(bugs, ext_bz_bug_id, bzcon)

    # A single post for jira.
    if jissues:
        # Sort the dict so the pipeline artifacts comment will be in a consistent order
        mr_issues = make_jissues(jissues, mrs=[])
        pipeline_urls = [bridge_job.ds_pipeline.web_url for bridge_job in bridge_jobs]
        update_testable_builds(mr_issues, comment_text, pipeline_urls)


def process_mr_pipeline(
        gl_project: 'Project',
        gl_mr: 'ProjectMergeRequest',
        branch: 'Branch'
        ) -> None:
    """Process the given pipeline and possibly post the results."""
    # If the MR doesn't have any BZs or JIssues listed, then we have nothing to do.
    mr_description = Description(gl_mr.description)
    if not mr_description.bugzilla and not mr_description.jissue:
        LOGGER.info('No bugs nor JIRA issues found in MR description, nothing to do.')
        return
    LOGGER.info('Found BZs %s and JIRA issues %s.', mr_description.bugzilla, mr_description.jissue)
    LOGGER.info("Branch '%s' uses pipelines: %s", branch.name, [b.name for b in branch.pipelines])

    local_pipeline = gl_project.pipelines.get(gl_mr.head_pipeline.get('id'))

    # Get data from all bridge jobs.
    bridge_jobs = fetch_bridge_jobs(local_pipeline.bridges.list())
    # Set the TargetedTestingMissing label if any valid builds are missing all_sources_targeted.
    set_targeted_testing_label(
        gl_project, gl_mr,
        any(job.builds_valid and not job.all_sources_targeted for job in bridge_jobs.values())
    )

    # Filter out jobs that we don't want to post to BZ/Jira.
    if not (bridge_jobs := filter_bridge_jobs(bridge_jobs, branch)):
        LOGGER.info('No ready BridgeJobs, nothing to do.')
        return

    # Actually do the work.
    header_str = HEADER % (gl_mr.title, gl_mr.web_url, local_pipeline.web_url)
    post_mr_pipelines(bridge_jobs.values(), mr_description.bugzilla, mr_description.jissue,
                      gl_mr.web_url, header_str)


def process_pipeline_event(body, session, **_):
    """Parse pipeline or merge_request msg and run _process_pipeline."""
    if body['object_kind'] == 'pipeline':
        # Some pipeline events are not associated with an MR. Ignore them.
        if body['merge_request'] is None:
            LOGGER.info('Pipeline %s is not associated with an MR, ignoring.',
                        body['object_attributes']['id'])
            return
        mr_id = body["merge_request"]["iid"]
    elif body['object_kind'] == 'merge_request':
        mr_id = body["object_attributes"]["iid"]

    gl_project = session.gl_instance.projects.get(body["project"]["path_with_namespace"])
    if not (gl_mr := common.get_mr(gl_project, mr_id)):
        return

    branch = session.rh_projects.get_target_branch(gl_mr.project_id, gl_mr.target_branch)

    if check_associated_mr(gl_mr):
        process_mr_pipeline(gl_project, gl_mr, branch)


def process_job_event(body, session, **_):
    """Process a job event from a downstream pipeline.

    We aim to update the BZs on successful setup jobs, before the testing finishes.
    """
    # Quickly filter out events we don't care about
    if body['build_status'] != 'success' or body['build_stage'] != 'setup':
        LOGGER.info("Ignoring event for pipeline %s with stage '%s' and status '%s'.",
                    body['pipeline_id'], body['build_stage'],
                    body['build_status'])
        return

    downstream_project = session.gl_instance.projects.get(body['project_id'])
    downstream_pipeline = downstream_project.pipelines.get(body['pipeline_id'])
    mr_url = get_variables(downstream_pipeline).get('mr_url')
    if not mr_url:   # No MR associated with the job/pipeline
        return

    namespace, mr_id = common.parse_mr_url(mr_url)

    mr_project = session.gl_instance.projects.get(namespace)
    if not (gl_mr := common.get_mr(mr_project, mr_id)):
        return

    branch = session.rh_projects.get_target_branch(gl_mr.project_id, gl_mr.target_branch)

    if check_associated_mr(gl_mr):
        process_mr_pipeline(mr_project, gl_mr, branch)


def process_mr_event(body, session, **_):
    """Process a merge request event message."""
    namespace = body['project']['path_with_namespace']
    mr_id = body["object_attributes"]["iid"]
    action = body['object_attributes'].get('action')
    LOGGER.info('Processing event for MR %s!%s (%s)', namespace, mr_id, action)

    mr_desc = body['object_attributes'].get('description', None)
    if mr_desc is None:
        project = session.gl_instance.projects.get(namespace)
        merge_request = common.get_mr(project, mr_id)
        mr_desc = merge_request.description if merge_request else None

    is_draft, draft_changed = common.draft_status(body)
    mr_desc_changed = 'description' in body.get('changes')

    # If the MR is a Draft we want to ignore it unless it just flipped to Draft state then
    # trick bugs_to_process() into giving us all the bugs to unlink.
    if is_draft:
        if draft_changed:
            LOGGER.info('MR changed to Draft, unlinking bugs.')
            action = 'close'
        else:
            LOGGER.info('MR is a Draft, ignoring.')
            return

    bz_jira_changes = common.bugs_to_process(mr_desc, action, body['changes'])
    if not any(bz_jira_changes.values()):
        LOGGER.info('No relevant bugs or jissues found in MR description.')
        return
    LOGGER.info('Found changes: %s', bz_jira_changes)

    # Has the Draft flag been removed? Or MR description changed? Then run the pipeline linker.
    if ((bz_jira_changes['link'] or bz_jira_changes['link_jissue']) and not is_draft) and \
            (draft_changed or mr_desc_changed):
        LOGGER.info('Sending MR event to pipeline processor.')
        process_pipeline_event(body, session)

    # If the event is not for a new or closing MR and does not correspond to some change in the MR
    # description or draft status then we can ignore it.
    if action not in ('open', 'close', 'reopen') and not draft_changed and not mr_desc_changed:
        LOGGER.info('MR has not changed, ignoring event.')
        return

    if bz_jira_changes['unlink_jissue'] and action == 'close':
        remove_gitlab_link_in_issues(mr_id, namespace, bz_jira_changes['unlink_jissue'])

    if not (bzcon := common.try_bugzilla_conn()):
        return

    update_bugzilla(bz_jira_changes, body['object_attributes']['url'], bzcon)


HANDLERS = {
    defs.GitlabObjectKind.MERGE_REQUEST: process_mr_event,
    defs.GitlabObjectKind.PIPELINE: process_pipeline_event,
    defs.GitlabObjectKind.BUILD: process_job_event
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('BUGLINKER')
    args = parser.parse_args(args)
    session = new_session('buglinker', args, HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
