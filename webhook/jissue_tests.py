"""Readiness tests for JIssue objects."""
# pylint: disable=invalid-name
from functools import wraps

from cki_lib.logger import get_logger

from webhook.defs import JIPriority
from webhook.defs import JIResolution
from webhook.defs import JIStatus
from webhook.defs import MrScope
from webhook.defs import MrState

LOGGER = get_logger('cki.webhook.jissue_tests')


# Decorator function that runs each test.
def test_runner(passed=MrScope.READY_FOR_MERGE, failed=MrScope.NEEDS_REVIEW, keep_going=True,
                skip_if_failed=None):
    """Decorator to run one JIssue Test."""
    def decorate_test(func):
        @wraps(func)
        def run_test(*args, **kwargs):
            jissue = kwargs['jissue'] if 'jissue' in kwargs else args[0]
            f_name = func.__name__

            if skip_if_failed and any(jissue.test_failed(test) for test in skip_if_failed):
                LOGGER.debug('Passing %s test as it has failed one of these tests: %s',
                             f_name, skip_if_failed)
                result = True
                result_str = 'skipped'
            else:
                result = func(*args, **kwargs)
                result_str = 'passed' if result else 'failed'
            if not result:
                jissue.failed_tests.append(f_name)

            scope = passed if result else failed
            LOGGER.debug('[%s] %s %s, scope is: %s', jissue.alias, f_name, result_str, scope.name)
            return result, scope, result if not keep_going else True
        return run_test
    return decorate_test


@test_runner()
def ParentCommitsMatch(jissue):
    """Pass if the commits referencing the JIssue in the parent MR and Dep MR match."""
    # If the dependency MR is merged then the dependant (parent) MR shouldn't have any commits which
    # reference this JIssue... unless it needs to be rebased >:( ...
    if jissue.mr.state is MrState.MERGED:
        return not jissue.parent_mr_commits
    # ... otherwise the commits which reference the JIssue should be the same!
    return jissue.commits == jissue.parent_mr_commits


ParentCommitsMatch.note = ("The commit SHAs referencing this JIRA Issue in this MR do not match "
                           "the commit SHAs referencing this JIRA Issue in the Dependency MR.  "
                           "This indicates this MR is based upon an older version of the "
                           "Dependency MR.")


@test_runner(failed=MrScope.READY_FOR_MERGE, keep_going=False,
             skip_if_failed=['ParentCommitsMatch'])
def MRIsNotMerged(jissue):
    """Pass if the Dependency JIssue's MR is not merged, otherwise "Fail"."""
    # Failing this isn't bad, it just gets us a note.
    return jissue.mr.state is not MrState.MERGED


MRIsNotMerged.note = "The MR associated with this Dependency JIRA Issue is already merged. Great."


@test_runner()
def InMrDescription(jissue):
    """Pass if the JIssue appears in the MR Description, otherwise Fail."""
    # Skip this test for any JIssue that doesn't have commits such as the dummy UNTAGGED JIssue.
    return jissue.in_mr_description if jissue.commits else True


InMrDescription.note = ("These commits have a `JIRA:` or `CVE:` tag which references "
                        "a JIRA Issue or CVE ID that was not listed in the merge request "
                        "description.  Please verify the tag is correct, or, add them to your MR "
                        "description as either a `JIRA`, `CVE`, or `Depends` tag.")


@test_runner()
def HasCommits(jissue):
    """Pass if the JIssue is tagged in some commits, otherwise Fail."""
    return bool(jissue.commits)


HasCommits.note = ("This tag is referenced in the MR description but is not referenced in "
                   "any of the MR's commits.  Please ensure the tag is correct and update "
                   "commit descriptions with `JIRA`, `Depends`, or `CVE` tags as needed.")


@test_runner(failed=MrScope.INVALID, keep_going=False)
def JIisNotUnknown(jissue):
    """Pass if the JIssue status is not UNKNOWN, otherwise Fail."""
    # On failure sets MR scope to INVALID and stops testing.
    return jissue.ji_status is not JIStatus.UNKNOWN


JIisNotUnknown.note = ("There was a problem retrieving data from JIRA. Please check "
                       "that the JIRA information is correct and contact a maintainer "
                       "if the problem persists.")


@test_runner(failed=MrScope.CLOSED, keep_going=False)
def JIisNotClosed(jissue):
    """Pass if the JIssue status is not CLOSED, otherwise Fail."""
    # On failure sets MR scope to INVALID and stops testing.
    return jissue.ji_status is not JIStatus.CLOSED


JIisNotClosed.note = ("This JIRA Issue's status is CLOSED.  Please check that the JIRA "
                      "information is correct.")


@test_runner(keep_going=False)
def NotUntagged(jissue):
    """Pass if the JIssue is not the faux 'UNTAGGED' jissue."""
    # On failure stops testing
    return not jissue.untagged


NotUntagged.note = ("No `JIRA` tag was found in these commits.  This project requires that "
                    "each commit have at least one `JIRA` tag.  Please double-check the tag "
                    "formatting and/or add a `JIRA: <issue_URL>` tag for each JIRA Issue.")


@test_runner()
def CveInMrDescription(jissue):
    """Pass if all CVEs associated with the JIssue are tagged in the MR Description."""
    result = True
    for cve_id in jissue.ji_cves:
        if cve := next((cve for cve in jissue.mr.cves if cve_id in cve.cve_ids), None):
            if cve.in_mr_description:
                continue
        result = False
        break
    return result


CveInMrDescription.note = ("This JIRA Issue is for a CVE that was not listed in the merge request "
                           "description.  Please verify the Issue's URL is correct or add the "
                           "CVE(s) to the MR description as a `CVE: CVE-YYYY-XXXXX` tag.")


# This test is Not Good because the graphql API does not expose per-commit file lists. So if an
# MR has a JIRA: INTERNAL tag anywhere then we can only validate whether the entire MR
# touches only internal files or not.
@test_runner()
def IsValidInternal(jissue):
    """Pass if the JIssue is a valid INTERNAL_JISSUE or not internal, otherwise Fail."""
    return jissue.mr.only_internal_files if jissue.internal else True


IsValidInternal.note = ("These commits are tagged as `INTERNAL` but were found to touch "
                        "source files outside of the redhat/ directory.  `INTERNAL` jissues "
                        "are only to be used for changes to files in the redhat/ directory.")


@test_runner()
def IsApproved(jissue):
    """Pass if the jissue policy check passed, otherwise Fail."""
    return bool(jissue.policy_check_ok[0])


IsApproved.note = "The JIRA Issue associated with these commits is not approved at this time."


@test_runner(failed=MrScope.READY_FOR_QA, skip_if_failed=['IsApproved'])
def IsVerified(jissue):
    """Return True if the JIRA Issue has Verified:Tested, otherwise False."""
    # Skip this test if IsApproved failed.
    return bool(jissue.ji_is_verified)


IsVerified.note = ("The JIRA Issue associated with these commits has not passed preverification at "
                   "this time.")


@test_runner()
def TargetReleaseSet(jissue):
    """Pass if Fix Version is set, otherwise fail."""
    # Skip this test for rhel-6 since those JIRA Issues don't use ITR/ZTR.
    if not jissue.ji:
        return True
    fvs = jissue.ji.fields.fixVersions
    if fvs and any(v.name.startswith('rhel-6') for v in fvs):
        return True
    return bool(jissue.ji_fix_version)


TargetReleaseSet.note = "This JIRA Issue has neither ITR nor ZTR set. Need at least one set."


@test_runner(skip_if_failed=['TargetReleaseSet'])
def CentOSZStream(jissue):
    """
    Pass if this is not the c9s project or the MR branch and JIRA Issue branch match.

    Fail if this is c9s and the ji_branch cannot be found or it has ZTR set.
    """
    result = True
    if jissue.is_dependency or jissue.mr.project.name != 'centos-stream-9' or not jissue.ji:
        pass
    elif not jissue.ji_branch or jissue.ji_branch.zstream_target_release:
        result = False
    return result


CentOSZStream.note = ("This JIRA Issue targets a zstream release but this MR exists in the Centos "
                      "Stream project.  Centos Stream Issues are expected to target ystream.  "
                      "This MR may need to be recreated in the RHEL9 project; please "
                      "contact a maintainer for further assistance.")


@test_runner(skip_if_failed=['CentOSZStream'])
def ComponentMatches(jissue):
    """Pass if any of the JIssue components matches the MR target branch, otherwise Fail."""
    if not jissue.ji:
        return True
    return bool(jissue.ji_components & jissue.mr.branch.components)


ComponentMatches.note = ("This JIRA Issue does not have any 'components' which match the MR's "
                         "target branch.  'kernel' Issues must have an MR which targets a kernel "
                         "branch and 'kernel-rt' Issue MRs must target a kernel-rt branch.")


@test_runner(skip_if_failed=['TargetReleaseSet', 'CentOSZStream', 'ComponentMatches'])
def BranchMatches(jissue):
    """Pass if the JIssue's MR Branch version matches the JIssue Branch version."""
    if not jissue.ji_branch or not jissue.mr.branch:
        return False
    return jissue.ji_branch.version == jissue.mr.branch.version


BranchMatches.note = ("This JIRA Issue has a Fix Version/s value that does not correspond to "
                      "the target branch of this MR.  Please review the Issue and the MR target "
                      "branch to ensure they are correct.")


@test_runner()
def CvePriority(cve):
    """Pass if the CVE priority is >= high and lead stream JIssue is on errata, otherwise Fail."""
    if cve.ji_priority < JIPriority.HIGH:
        return True
    parent_branch = cve.parent_mr.branch
    # Make a reversed copy of the CVE's ji_depends_on list since we want the 'highest' branch first.
    cve_clones = list(reversed(cve.ji_depends_on))
    # The lead stream clone for rhel-6 & rhel-7 is the one associated with the 'main' branch,
    # otherwise it is the first one not associated with the 'main' branch.
    LOGGER.warning("Parent branch project: %s", parent_branch.project.name)
    if parent_branch.project.name in ('rhel-6', 'rhel-7'):
        lead_clone = next((jissue for jissue in cve_clones if jissue.ji_branch.name == 'main'))
    else:
        lead_clone = next((jissue for jissue in cve_clones if jissue.ji_branch.name != 'main'))
    # The clone associated with the MR.
    parent_clone = next((jissue for jissue in cve_clones if jissue.ji_branch == parent_branch))
    if lead_clone.ji_branch > parent_clone.ji_branch and \
       lead_clone.ji_resolution != JIResolution.ERRATA:
        LOGGER.warning("lcb: %s, pcb: %s, lcr: %s", lead_clone.ji_branch, parent_clone.ji_branch,
                       lead_clone.ji_resolution)
        return False
    return True


CvePriority.note = "This CVE is not yet in errata for the lead stream."


# Tests to run on JIRA: Issues
ISSUE_TESTS = [InMrDescription,
               HasCommits,
               JIisNotUnknown,
               JIisNotClosed,
               TargetReleaseSet,
               CveInMrDescription,
               IsValidInternal,
               IsApproved,
               IsVerified,
               CentOSZStream,
               ComponentMatches,
               BranchMatches
               ]

# Tests to run on Depends: Issues
DEP_TESTS = [ParentCommitsMatch,
             MRIsNotMerged,
             InMrDescription,
             HasCommits,
             JIisNotUnknown,
             JIisNotClosed,
             TargetReleaseSet,
             IsValidInternal,
             IsApproved,
             IsVerified,
             CentOSZStream,
             ComponentMatches,
             ]

# Tests to run on CVE: Issues
CVE_TESTS = [InMrDescription,
             HasCommits,
             JIisNotUnknown,
             # JIisNotClosed,
             # CvePriority
             ]

INTERNAL_TESTS = [InMrDescription,
                  HasCommits,
                  IsValidInternal]

UNTAGGED_TESTS = [NotUntagged]
