"""Commit Policy Checker.

Set of simple functions for dist-git policy expression evaluation as presented
at http://pkgs.devel.redhat.com/rules.html.

It uses RPN (Reverse Polish Notation) aka postfix notation for evaluation and
performs some basic syntax checks, which should be enough.
"""

from enum import IntEnum
from enum import auto
from re import compile as re_compile

from bs4 import BeautifulSoup
from cki_lib.logger import get_logger
from cki_lib.misc import get_env_var_or_raise
from cki_lib.session import get_session

from webhook.defs import BZStatus
from webhook.defs import JIStatus
from webhook.libjira import JiraField

LOGGER = get_logger(__name__)


class TokenType(IntEnum):
    """Operators for Token processing."""

    # Operators
    OR = auto()
    AND = auto()
    EQ = auto()

    LPAR = auto()
    RPAR = auto()
    VAR = auto()
    LIT = auto()
    EXP = auto()


class Token:
    """Lexical token representation used for RPN evaluation."""

    # pylint: disable=too-few-public-methods

    def __init__(self, token_type, name, pos=None):
        """Initialize Token instance."""
        self.type = token_type
        self.name = name
        self.value = ''
        self.offset = -1

        if token_type is TokenType.LIT:
            self.value = name

        if pos:
            self.offset = pos - len(name)


def tokenize(policy):
    """Parse input dist-git policy string and return list of tokens."""
    # pylint: disable=too-many-branches
    tokens = []
    lexeme = ''
    pos = 0
    separators = True

    for pol_element in policy:
        pos += 1

        if separators and pol_element in [' ', '(', ')', '\n']:
            token_type = None

            if lexeme == 'or':
                token_type = TokenType.OR
            elif lexeme == 'and':
                token_type = TokenType.AND
            elif lexeme == '==':
                token_type = TokenType.EQ
            elif lexeme:
                if tokens and tokens[-1].type is TokenType.EQ:
                    token_type = TokenType.LIT
                    if lexeme == 'Approved' and pol_element == ' ':
                        lexeme += ' '
                        continue
                else:
                    token_type = TokenType.VAR

            if token_type:
                tokens.append(Token(token_type, lexeme, pos - 1))

            lexeme = pol_element

            if lexeme == '(':
                tokens.append(Token(TokenType.LPAR, lexeme, pos))
            elif lexeme == ')':
                tokens.append(Token(TokenType.RPAR, lexeme, pos))

            lexeme = ''

            continue

        lexeme += pol_element

        if lexeme == 'jiraField':
            separators = False
            continue

        if not separators and pol_element == ')':
            separators = True

    if lexeme:
        if tokens and tokens[-1].type == TokenType.EQ:
            token_type = TokenType.LIT
        else:
            token_type = TokenType.VAR
        tokens.append(Token(token_type, lexeme, pos))

    return tokens


def rpn(tokens):
    """Return list of tokens in RPN.

    Input is list of tokens as returned by the tokenize function.
    """
    # pylint: disable=too-many-branches
    rpn_list = []
    stack = []
    ops = [TokenType.OR, TokenType.AND, TokenType.EQ]
    token = None

    for token in tokens:
        if token.type in ops:
            while stack:
                if stack[-1].type not in ops:
                    break
                if stack[-1].type < token.type:
                    break
                if stack[-1].type is TokenType.EQ:
                    if len(rpn_list) < 2:
                        raise Exception(
                                f'Comparison requires two operands\n{dump_tokens([stack[-1]])}')
                    if rpn_list[-1].type is not TokenType.LIT:
                        raise Exception(
                                f'Second "==" operand has to be LIT\n{dump_tokens([stack[-1]])}')
                    if rpn_list[-2].type is not TokenType.VAR:
                        raise Exception(
                                f'First "==" operand has to be VAR\n{dump_tokens([stack[-1]])}')
                rpn_list.append(stack.pop())

            stack.append(token)

        elif token.type is TokenType.LPAR:
            stack.append(token)

        elif token.type is TokenType.RPAR:
            while stack and stack[-1].type is not TokenType.LPAR:
                rpn_list.append(stack.pop())

            if not stack:
                raise Exception(f'Missing left bracket for token\n{dump_tokens([token])}')

            stack.pop()

        else:
            rpn_list.append(token)

    while stack:
        if stack[-1].type not in ops:
            raise Exception(f'Expecting EQ,OR,AND token\n{dump_tokens([stack[-1]])}')
        if stack[-1].type is TokenType.EQ:
            if len(rpn_list) < 2:
                raise Exception(f'Comparison requires two operands\n{dump_tokens([stack[-1]])}')
            if rpn_list[-1].type is not TokenType.LIT:
                raise Exception(f'Second "==" operand has to be LIT\n{dump_tokens([stack[-1]])}')
            if rpn_list[-2].type is not TokenType.VAR:
                raise Exception(f'First "==" operand has to be VAR\n{dump_tokens([stack[-1]])}')
        rpn_list.append(stack.pop())

    return rpn_list


def evaluate(tokens, failed=None, steps=None, is_jira=False):
    """Evaluate expression represented by tokens in RPN as returned by the rpn function.

    If failed list param is set, it will contain failed bugzilla expressions. If steps list
    param is set, it will contain intermediate expressions used during evaluation. This is
    for debug purposes. Return True or False as a result of expression evaluation.
    """
    # pylint: disable=too-many-branches,too-many-statements
    stack = []
    bz_fields = ('release', 'zstream', 'blocker', 'exception',
                 'internal_target_release', 'zstream_target_release')

    for token in tokens:
        if token.type in [TokenType.LPAR, TokenType.RPAR]:
            raise Exception(f'Invalid token type\n{dump_tokens([token])}')

        if token.type in [TokenType.LIT, TokenType.VAR]:
            stack.append(token)
            continue

        if len(stack) < 2:
            raise Exception(f'Missing operand(s) for token\n{dump_tokens([token])}')

        # first operand on the stack has to be literal or expression
        op1 = stack.pop()
        if op1.type not in [TokenType.LIT, TokenType.EXP]:
            raise Exception(f'Expecting LIT,EXP token\n{dump_tokens([op1])}')

        # second operand on the stack has to be variable or expression
        op2 = stack.pop()
        if op2.type not in [TokenType.VAR, TokenType.EXP]:
            raise Exception(f'Expecting VAR,EXP token\n{dump_tokens([op2])}')

        # token with intermediate expression result
        res = Token(TokenType.EXP,
                    f'({op1.name}[{op1.value}] {token.name} {op2.name}[{op2.value}])')
        res.value = False
        res.offset = token.offset

        # do the evaluation
        if token.type is TokenType.OR:
            if op1.value or op2.value:
                res.value = True
        elif token.type is TokenType.AND:
            if op1.value and op2.value:
                res.value = True
        elif token.type is TokenType.EQ:
            if op1.value == op2.value:
                res.value = True

        stack.append(res)
        if steps is not None:
            steps.append(res)

        # store failed '==' expressions for bugzilla
        while True:
            if failed is None:
                break
            if res.value:
                break
            if op2.type is not TokenType.VAR:
                break
            # don't report failed jira expressions
            if op2.name.startswith('jira') and not is_jira:
                break
            if is_jira and op2.name in bz_fields:
                break
            if token.type is not TokenType.EQ:
                break
            failed.append(f'{op2.name}="{op2.value}" [need: "{op1.value}"]')
            break

    if len(stack) != 1:
        raise Exception(f'Unexpected stack content\n{dump_tokens(stack)}')

    res = stack.pop()
    return res.value


def errexp(tokens, is_jira=False):
    """Return string representing the original expression extented by variable values.

    This should hopefully provide a user friendly way how to review failed expression and find out
    what conditions are not met. For better readability, by default, jira related variables are
    omitted. This can be changed by setting "is_jira" to True. The list of input tokens has
    to be the same as passed to the evaluate function. Meaning tokens in RPN and with variables
    values set.
    """
    # pylint: disable=too-many-branches,too-many-statements
    stack = []
    jira_exp = True

    for token in tokens:
        if token.type is TokenType.LIT:
            val = f'"{token.value}"'
            stack.append((not jira_exp, TokenType.LIT, val))
            continue

        if token.type is TokenType.VAR:
            val = f'{token.name} "{token.value}"'
            if token.name.startswith('jira'):
                stack.append((jira_exp, TokenType.VAR, val))
            else:
                stack.append((not jira_exp, TokenType.VAR, val))
            continue

        exp1_is_jira, exp1_type, exp1 = stack.pop()
        exp2_is_jira, exp2_type, exp2 = stack.pop()

        # If requested, by is_jira == False, remove jira
        # operator from "or" expression, but only if the other
        # operator is not jira related.
        if token.type is TokenType.OR:
            val1 = val2 = ""
            if exp1_type not in [TokenType.EQ, TokenType.OR]:
                val1 = f'({exp1})'
            else:
                val1 = exp1

            if exp2_type not in [TokenType.EQ, TokenType.OR]:
                val2 = f'({exp2})'
            else:
                val2 = exp2

            val = f'{val2} or {val1}'

            if exp1_is_jira and exp2_is_jira:
                stack.append((jira_exp, TokenType.OR, val))

            elif not exp1_is_jira and not exp2_is_jira:
                stack.append((not jira_exp, TokenType.OR, val))

            elif exp1_is_jira and not is_jira:
                stack.append((not jira_exp, exp2_type, exp2))

            elif exp2_is_jira and not is_jira:
                stack.append((not jira_exp, exp1_type, exp1))

            elif not exp1_is_jira and is_jira:
                stack.append((not jira_exp, exp2_type, exp2))

            elif not exp2_is_jira and is_jira:
                stack.append((not jira_exp, exp1_type, exp1))

            else:
                stack.append((jira_exp, TokenType.OR, val))

        elif token.type is TokenType.AND:
            val1 = val2 = ""
            if exp1_type not in [TokenType.EQ, TokenType.AND]:
                val1 = f'({exp1})'
            else:
                val1 = exp1

            if exp2_type not in [TokenType.EQ, TokenType.AND]:
                val2 = f'({exp2})'
            else:
                val2 = exp2

            val = f'{val2} and {val1}'
            if exp1_is_jira and exp2_is_jira:
                stack.append((jira_exp, TokenType.AND, val))
            else:
                stack.append((not jira_exp, TokenType.AND, val))

        elif token.type is TokenType.EQ:
            val = f'{exp2} == {exp1}'
            if exp2_is_jira:
                stack.append((jira_exp, TokenType.EQ, val))
            else:
                stack.append((not jira_exp, TokenType.EQ, val))

    _, _, val = stack.pop()
    return val


def dump_tokens(tokens, header=True):
    """Return string/table representation of input token list."""
    text = ''

    if header:
        text += f'OFFSET TYPE {"VALUE":<35} NAME\n'
    for token in tokens:
        text += f'{token.offset:06} {token.type.name:4} {token.value:<35} {token.name}\n'

    return text.rstrip()


def _tokenize_string(policy_string):
    """Return a list of Tokens derived from the given string."""
    try:
        return rpn(tokenize(policy_string))
    except Exception:  # pylint: disable=broad-except
        LOGGER.warning('Could not tokenize policy string %s', policy_string)
    return []


def _get_policy_lines(raw_policies):
    """Return a dict with branch name as key and policy string as value."""
    # The policy txt file has the branch name by itself and then a new line indented with two spaces
    # that contains the policy string we want.
    policies = {}
    branch = ''
    for line in raw_policies.splitlines():
        if not line.startswith('  '):
            branch = line.strip().lstrip('*')
        else:
            # Some branches have some sort of secondary policy whose title is indented as well and
            # we want to ignore this.
            if branch not in policies:
                policies[branch] = line.strip()
    return policies


def _get_policy_divs(raw_policies):
    """Return a BeautifulSoup thing."""
    return BeautifulSoup(raw_policies, 'html.parser').find_all('div', class_='rule')


def _get_name_from_div(div):
    """Return the branch name from a div."""
    return div.dt.get_text().strip().lstrip('*')


def _get_policy_from_div(div):
    """Transform a policy div into a list of tokens, handling any Exceptions."""
    return _tokenize_string(div.dd.get_text())


def _parse_raw_policies(raw_policies, raw_type, policy_regex):
    """Return a dict with policy name keys mapped to their list of tokens."""
    policies = {}
    if policy_regex:
        policy_regex = re_compile(policy_regex)
    match raw_type:
        case 'text/plain':
            for branch, policy_string in _get_policy_lines(raw_policies).items():
                if not policy_regex or policy_regex.match(branch):
                    policies[branch] = _tokenize_string(policy_string)
        case 'text/html':
            for div in _get_policy_divs(raw_policies):
                branch = _get_name_from_div(div)
                if not policy_regex or policy_regex.match(branch):
                    policies[branch] = _get_policy_from_div(div)
    return policies


def _get_response(policy_url):
    """Return the response object for the given policy url."""
    session = get_session('cki.webhook.cpc')
    response = session.get(policy_url)
    session.close()
    response.raise_for_status()  # raise an error if we didn't get a nice response
    return response


def _fetch_raw_policies(policy_url):
    """Return the raw policy with type."""
    response = _get_response(policy_url)
    return response.text, response.headers['Content-Type'].split(';')[0]


def get_policy_data(policy_regex=None):
    """Fetch raw policy data and return a dict mapping branch names to token lists."""
    # If policy_regex is set, only policies whose name matches the regex are returned.
    LOGGER.debug('Loading policies.')
    raw_policies, raw_type = _fetch_raw_policies(get_env_var_or_raise('COMMIT_POLICY_URL'))
    if not (policies := _parse_raw_policies(raw_policies, raw_type, policy_regex)):
        raise RuntimeError('Problem loading policy data.')
    LOGGER.info('Found %d policies.', len(policies))
    return policies


def get_one_policy(branch_name):
    """Return the list of tokens for the matching branch, or None."""
    raw_policies, raw_type = _fetch_raw_policies(get_env_var_or_raise('COMMIT_POLICY_URL'))
    match raw_type:
        case 'text/plain':
            for branch, policy_string in _get_policy_lines(raw_policies).items():
                if branch == branch_name:
                    return _tokenize_string(policy_string)
        case 'text/html':
            for div in _get_policy_divs(raw_policies):
                if _get_name_from_div(div) == branch_name:
                    return _get_policy_from_div(div)
    return None


def get_bz_itr(bugdata):
    """Get the Internal Target Release from bugzilla data."""
    return getattr(bugdata, 'cf_internal_target_release', '')


def get_bz_ztr(bugdata):
    """Get the Z-Stream Target Release from bugzilla data."""
    return getattr(bugdata, 'cf_zstream_target_release', '')


def parse_bz_flag_data(flags):
    """Parse bug flag data into a simple dict of name/status pairs."""
    return {flag['name']: flag['status'] for flag in flags}


def _load_tokens(bug, tokens):
    """Populate token values from a bug's data."""
    flags = parse_bz_flag_data(bug.flags)
    for token in tokens:
        if token.type is not TokenType.VAR:
            continue
        token.value = ''  # We don't want to evaluate any old values!
        match token.name:
            case 'internal_target_release':
                token.value = get_bz_itr(bug)
                continue
            case 'zstream_target_release':
                token.value = get_bz_ztr(bug)
                continue
            case token.name if token.name in flags:
                token.value = flags[token.name]


def is_bug_ready(bug, tokens, get_errexp=True, product=None, component=None, status=None):
    # pylint: disable=too-many-arguments
    """Return True if bug passes policy check. tokens must be a valid list of Token objects.

    If get_errexp=True, the second return value is a string with the return value of errexp().
    If get_errexp=False, the second return value is a list populated by evaluate().

    If product and/or component are set, cpc will return failure if the input product/component
    does not match the bug.

    If status is set to a BZStatus then the bug must have at least this status.
    """
    _load_tokens(bug, tokens)
    eval_output = []
    try:
        eval_result = evaluate(tokens, failed=eval_output, is_jira=True)
    except Exception:  # pylint: disable=broad-except
        token_dump = dump_tokens(tokens)
        LOGGER.warning('Failed to evaluate tokens for Bug %s:\n%s', bug.id, token_dump)
        if get_errexp:
            eval_output = token_dump
        else:
            eval_output.append(token_dump)
        return False, eval_output
    product_match = not (product and bug.product != product)
    component_match = not (component and bug.component != component)
    status_passes = not (status and BZStatus.from_str(bug.status) < status)
    if not product_match or not component_match or not status_passes:
        eval_result = False
    if get_errexp:
        eval_output = errexp(tokens) if not eval_result else ''
        if product and not product_match:
            eval_output += f' and product "{bug.product}" == "{product}"'
        if component and not component_match:
            eval_output += f' and component "{bug.component}" == "{component}"'
        if status and not status_passes:
            eval_output += f' and status "{bug.status}" >= "POST"'
    return eval_result, eval_output


def get_jira_fix_version(jissue):
    """Extract JIRA Issue's fixVersions field."""
    fix_versions = jissue.fields.fixVersions
    if not fix_versions:
        return ""
    if len(fix_versions) > 1:
        return ""

    return fix_versions[0].name


def _load_jira_tokens(issue, tokens):
    """Populate token values from a JIRA Issue's data."""
    for token in tokens:
        if token.type is not TokenType.VAR:
            continue
        token.value = ''  # We don't want to evaluate any old values!
        match token.name:
            case 'jiraField(Release Blocker)':
                token.value = f'{getattr(issue.fields, JiraField.Release_Blocker)}'
                continue
            case 'jiraProject':
                token.value = str(issue.fields.project)
                continue
            case 'jiraField(fixVersions)':
                token.value = get_jira_fix_version(issue)
                continue


def get_product_from_jira_versions(jissue):
    """Extract target product from jira fix versions field."""
    fix_versions = jissue.fields.fixVersions
    if fix_versions is None:
        return "(empty)"
    if len(fix_versions) > 1:
        return "(multiple)"

    pname = ""
    major = 0
    for version in fix_versions:
        if version.name.startswith("rhel-"):
            pname = "Red Hat Enterprise Linux"
            major = version.name.strip("rhel-").split(".")[0]

    if pname:
        return f"{pname} {major}"

    return "(invalid)"


def is_jissue_ready(jissue, tokens, get_errexp=True, product=None, component=None, status=None):
    # pylint: disable=too-many-arguments
    """Return True if jissue passes policy check. tokens must be a valid list of Token objects.

    If get_errexp=True, the second return value is a string with the return value of errexp().
    If get_errexp=False, the second return value is a list populated by evaluate().

    If product and/or component are set, cpc will return failure if the input product/component
    does not match the jissue.

    If status is set to a JIStatus then the jissue must have at least this status.
    """
    _load_jira_tokens(jissue, tokens)
    eval_output = []
    try:
        eval_result = evaluate(tokens, failed=eval_output, is_jira=True)
    except Exception:  # pylint: disable=broad-except
        token_dump = dump_tokens(tokens)
        LOGGER.warning('Failed to evaluate tokens for JIssue %s:\n%s', jissue.id, token_dump)
        if get_errexp:
            eval_output = token_dump
        else:
            eval_output.append(token_dump)
        return False, eval_output
    target_product = get_product_from_jira_versions(jissue)
    product_match = not (product and target_product != product)
    component_match = False
    if component:
        for jcomp in jissue.fields.components:
            # we have to extract the first element from the components field, due to jira's flat
            # structure for components and sub-components
            if jcomp.name.split("/")[0].strip() == component:
                component_match = True
                break
    else:
        component_match = True
    status_passes = not (status and JIStatus.from_str(jissue.status) < status)
    if not product_match or not component_match or not status_passes:
        LOGGER.warning("pm: %s, cm: %s, sp: %s", product_match, component_match, status_passes)
        eval_result = False
    if get_errexp:
        eval_output = errexp(tokens, is_jira=True) if not eval_result else ''
        if product and not product_match:
            eval_output += (f' and FV mapping "{target_product}" does not match "{product}"')
        if component and not component_match:
            component_prefix = jissue.fields.components[0].name.split("/")[0].strip()
            eval_output += f' and component prefix "{component_prefix}" != "{component}"'
        if status and not status_passes:
            eval_output += f' and status "{jissue.fields.status}" >= "IN_PROGRESS"'
    return eval_result, eval_output
