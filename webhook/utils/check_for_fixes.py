#!/usr/bin/env python
"""Look for upstream Fixes: commits that we need to consider backporting."""

import argparse
import os
import subprocess

from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_instance
import sentry_sdk

from webhook import defs
from webhook.fixes import FixesMR
from webhook.fixes import report_results
from webhook.graphql import GitlabGraph
from webhook.rh_metadata import Projects

LOGGER = logger.get_logger('webhook.utils.check_for_fixes')

# Get all open MRs.
MR_QUERY = """
query mrData($namespace: ID!, $first: Boolean = true, $after: String = "") {
  %s(fullPath: $namespace) {
    id @include(if: $first)
    mergeRequests(state: opened, after: $after) {
      pageInfo {hasNextPage endCursor}
      nodes {iid webUrl project{name fullPath} draft}
    }
  }
}
"""


def _git(kernel_src, git_args, **kwargs):
    """Run a git via subprocess (for things GitPython doesn't support)."""
    kwargs.setdefault('check', True)
    # pylint: disable=subprocess-run-check
    LOGGER.debug("Running command: git %s", git_args)
    return subprocess.run(['git'] + git_args.split(), cwd=kernel_src,
                          capture_output=True, text=True, **kwargs)


def get_open_mrs(graphql, namespace, namespace_type):
    """Return a list of the MR objects we're interested in."""
    result = graphql.check_query_results(
        graphql.client.query(MR_QUERY % namespace_type,
                             variable_values={'namespace': namespace},
                             paged_key=f'{namespace_type}/mergeRequests'),
        check_keys={namespace_type})
    if result and result[namespace_type] is None:
        raise RuntimeError(f"Namespace '{namespace}' is not visible!")
    mrs = {}
    for mreq in misc.get_nested_key(result, f'{namespace_type}/mergeRequests/nodes'):
        namespace = mreq['project']['fullPath']
        if namespace in mrs:
            mrs[namespace].append(mreq)
            continue
        mrs[namespace] = [mreq]

    LOGGER.debug('%s MRs: found %s', namespace_type.title(), mrs)
    return mrs


def _get_parser_args():
    parser = argparse.ArgumentParser(description='Check for upstream Fixes against open MRs')

    # Global options
    parser.add_argument('-g', '--groups', default=os.environ.get('GL_GROUPS', '').split(),
                        help='gitlab groups to fetch MRs from.', nargs='+', required=False)
    parser.add_argument('-p', '--projects', default=os.environ.get('GL_PROJECTS', '').split(),
                        help='gitlab projects to fetch MRs from.', nargs='+', required=False)
    parser.add_argument('-l', '--linux-src', default=os.environ.get('LINUX_SRC', ''),
                        help='Directory containing upstream Linux kernel source git tree')
    parser.add_argument('-T', '--testing', action='store_true', default=False,
                        help="Run in testing mode: do not update gitlab labels")
    parser.add_argument('--sentry-ca-certs', default=os.environ.get('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    return parser.parse_args()


def main(args):
    """Find open MRs, check for relevant upstream Fixes."""
    if not args.linux_src:
        LOGGER.warning("No valid Linux source git found, aborting!")
        return

    LOGGER.info("Fetching latest upstream git commits...")
    _git(args.linux_src, 'pull origin master')
    try:
        _git(args.linux_src, 'fetch --all')
    except subprocess.CalledProcessError:
        pass

    LOGGER.info('Finding open MRs in %s %s...', args.groups, args.projects)
    graphql = GitlabGraph()
    gl_instance = get_instance(defs.GITFORGE)

    mrs_to_check = {}
    for group in args.groups:
        mrs_to_check.update(get_open_mrs(graphql, group, 'group'))
    for project in args.projects:
        mrs_to_check.update(get_open_mrs(graphql, project, 'project'))

    LOGGER.debug("MR lists: %s", mrs_to_check)
    if not mrs_to_check:
        LOGGER.info('No open MRs to process.')
        return

    gl_projects = {}
    for namespace, mrs in mrs_to_check.items():
        if not (gl_project := gl_projects.get(namespace)):
            gl_project = gl_instance.projects.get(namespace)
            gl_projects[namespace] = gl_project
        for mreq in mrs:
            params = {'graphql': graphql, 'gl_instance': gl_instance, 'projects': Projects(),
                      'url': mreq['webUrl'], 'kernel_src': args.linux_src}
            fixes_mr = FixesMR(**params)
            report_results(fixes_mr, graphql.username, mreq)


if __name__ == '__main__':
    pargs = _get_parser_args()
    misc.sentry_init(sentry_sdk, ca_certs=pargs.sentry_ca_certs)
    main(pargs)
