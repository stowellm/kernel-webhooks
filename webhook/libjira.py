"""Library for interacting with jira."""
from datetime import datetime
from enum import StrEnum
from enum import unique
from os import environ
from typing import Iterable
from typing import Optional
from typing import Union

from cki_lib.logger import get_logger
from cki_lib.misc import is_production_or_staging
from jira import JIRA
from jira.exceptions import JIRAError
from jira.resources import Issue

from webhook.defs import GITFORGE
from webhook.defs import JIRA_BOT_ACCOUNTS
from webhook.defs import JIRA_SERVER
from webhook.defs import JIStatus
from webhook.defs import JPFX
from webhook.defs import READY_FOR_QA_LABEL

LOGGER = get_logger('cki.webhook.libjira')


@unique
class JiraField(StrEnum):
    # pylint: disable=invalid-name
    """Map pretty names to backend field name values."""

    assignee = 'assignee'
    components = 'components'
    fixVersions = 'fixVersions'
    issuelinks = 'issuelinks'
    labels = 'labels'
    priority = 'priority'
    project = 'project'
    reporter = 'reporter'
    status = 'status'
    subtasks = 'subtasks'
    summary = 'summary'
    updated = 'updated'
    resolution = 'resolution'
    Preliminary_Testing = 'customfield_12321540'
    Testable_Builds = 'customfield_12321740'
    Release_Blocker = 'customfield_12319743'

    @classmethod
    def all(cls):
        """Return the list of all the field values."""
        return [field.value for field in cls]


def connect_jira(token_auth=environ.get('JIRA_TOKEN_AUTH'), host=None):
    """Connect to JIRA and return a JIRA connection object."""
    if not host:
        host = environ.get('JIRA_SERVER', JIRA_SERVER)
    jira_options = {'server': host}
    jiracon = JIRA(options=jira_options, token_auth=token_auth)
    return jiracon


def _clear_issue_cache(issue_cache):
    """Clear the issue_cache list."""
    LOGGER.info('Clearing %s issues from the cache.', len(issue_cache))
    issue_cache.clear()


def _update_issue_cache(new_issues, issue_cache):
    """Put the new_issues in the issue_cache."""
    for issue in new_issues:
        if issue.id in issue_cache:
            LOGGER.warning('JIRA Issue %s already in issue_cache.', issue.key)
        issue_cache[issue.id] = issue


def _getissues(jira: JIRA,
               issues: Optional[Iterable[str]] = None,
               cves: Optional[Iterable[str]] = None
               ) -> list[Issue]:
    """Return jira Issues matching the given list of issue keys or CVE labels."""
    if not issues and not cves:
        raise ValueError('Search lists are empty.')
    labels_str = f"labels in ({', '.join(cves)})" if cves else ''
    issues_str = ' OR '.join(f'key={issue}' for issue in issues) if issues else ''
    joiner_str = ' OR ' if labels_str and issues_str else ''
    comps_str = 'component in componentMatch("^(kernel|kernel-rt|kernel-automotive)($| / .*$)")'
    jql_str = f"project = {JPFX.rstrip('-')} AND {comps_str} AND "
    jql_str += f'({labels_str}{joiner_str}{issues_str})'
    LOGGER.info('Fetching Jira data for issues: %s, cves: %s, JQL: %s', issues, cves, jql_str)
    # This will catch unknown issues but not unknown CVEs.
    try:
        issues = jira.search_issues(jql_str, fields=JiraField.all())
    except JIRAError:
        LOGGER.warning("User specified invalid jira issue(s): %s", issues)
        issues = []
    return issues


def parse_search_list(search_list: Iterable[str]) -> tuple[set, set]:
    """Split the given search_list items into a tuple of jira Issues and CVE names."""
    issues: set[str] = set()
    cves: set[str] = set()
    other = []
    for item in search_list:
        if isinstance(item, str):
            if item.startswith(JPFX):
                issues.add(item)
            elif item.startswith('CVE-'):
                cves.add(item)
        else:
            other.append(item)
    if other:
        raise ValueError(f'Unknown search term: {other}')
    return issues, cves


def fetch_issues(search_list: Iterable[str], jira: Optional[JIRA] = None) -> list[Issue]:
    # pylint: disable=dangerous-default-value
    """Return a list of issue objects queried from JIRA."""
    if not search_list:
        raise ValueError('Search list is empty.')
    issues, cves = parse_search_list(search_list)
    if not jira:
        jira = connect_jira()
    return _getissues(jira, issues=issues, cves=cves)


def issues_with_lower_status(issue_list, status, min_status=JIStatus.NEW):
    """Return the list of issues that have a status lower than the input status."""
    return [issue for issue in issue_list
            if min_status <= JIStatus.from_str(issue.fields.status) < status]


def latest_testing_failed_timestamp(jira, issue):
    """Return most recent timestamp in history when Preliminary Testing field was set to Fail."""
    timestamp = None
    comments = reversed(jira.comments(issue))
    for comment in comments:
        c_detail = jira.comment(issue.id, comment.id)
        if "issued failed testing" in c_detail.body:
            timestamp = datetime.strptime(c_detail.created, '%Y%m%dT%H:%M:%S')
            LOGGER.debug('Preliminary Testing: Fail last added %s', timestamp)
            return timestamp
    LOGGER.warning('Did not find issue failed testing in comment history.')
    return None


def mark_issues_as_ready_for_testing(issue_list, labels):
    """Check for issues that should be marked as Preliminary Testing: Requested."""
    if READY_FOR_QA_LABEL not in labels:
        LOGGER.debug("Merge request not ready for QA")
        return

    for issue in issue_list:
        if prelim_testing := getattr(issue.fields, JiraField.Preliminary_Testing):
            LOGGER.debug("Issue %s already set to %s", issue.key, prelim_testing.value)
        else:
            LOGGER.info("Setting Preliminary Testing field to Requested")
            if is_production_or_staging():
                issue.update(fields={JiraField.Preliminary_Testing: {'value': 'Requested'}})


def issues_to_move_to_in_progress(issue_list, mr_pipeline_timestamp):
    """Return the issues from the input list that need to be moved to In Progress."""
    # For any input issue with FailedQA we check the timestamp when FailedQA was last added to the
    # issue against the mr_pipeline_timestamp and if the former is newer we pop it from the
    # ji_to_update list.
    # mr_pipeline_timestamp is the datetime when the MR's head pipeline finished, if any.
    ji_to_update = issues_with_lower_status(issue_list, JIStatus.IN_PROGRESS)
    if not (ji_to_check := [issue for issue in ji_to_update if
                            getattr(issue.fields, JiraField.Preliminary_Testing) and
                            getattr(issue.fields, JiraField.Preliminary_Testing).value == "Fail"]):
        return ji_to_update

    if not mr_pipeline_timestamp:
        raise ValueError("The MR doesn't have a completed pipeline but an issue has "
                         "Preliminary Testing: Fail?")

    for issue in ji_to_check:
        if failed_qa_timestamp := latest_testing_failed_timestamp(issue_list[0].jira, issue):
            if failed_qa_timestamp > mr_pipeline_timestamp:
                LOGGER.debug('issue %s got Preliminary Testing: Fail after latest pipeline.',
                             issue.key)
                ji_to_update.remove(issue)
    return ji_to_update


def add_gitlab_link_in_issues(issues, this_mr):
    """Add GitLab MR link to Issue Links field in JIRA Issues."""
    base_url = f'{GITFORGE}/{this_mr.namespace}'
    mr_link = f'{base_url}/-/merge_requests/{this_mr.mr_id}'
    title = f'Merge Request: {this_mr.title}'
    icon = (f'{GITFORGE}/assets/favicon-'
            '72a2cad5025aa931d6ea56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png')
    gitlab_link = {'url': mr_link, 'title': title,
                   'icon': {'url16x16': icon, 'title': 'GitLab Merge Request'}}
    comment = (f"Red Hat's GitLab kernel-webhooks bot linked [a merge request|{mr_link}] to "
               "this issue.\n"
               f"*Title:* {this_mr.title}\n"
               f"*Project:* [{this_mr.project.name}|{base_url}], "
               f"*Target Branch:* [{this_mr.branch.name}|{base_url}/-/tree/{this_mr.branch.name}], "
               f"*Merge Request #:* [{this_mr.mr_id}|{mr_link}].")

    jira = connect_jira()
    for issue in issues:
        current_links = jira.remote_links(issue)
        link_exists = False
        for link in current_links:
            link_detail = jira.remote_link(issue=issue, id=link.id)
            if link_detail.object.url == mr_link:
                link_exists = True
                break
        if link_exists:
            LOGGER.info("MR %s already linked in %s", this_mr.mr_id, issue.key)
        else:
            LOGGER.info("Linking [%s](%s) to issue %s", title, mr_link, issue.key)
            if is_production_or_staging():
                jira.add_simple_link(issue=issue, object=gitlab_link)
                jira.add_comment(issue.key, comment)


def remove_gitlab_link_comment_in_issue(jira, issue, mr_url):
    """Remove the comment we left in the issue pointing to the MR."""
    for comment in jira.comments(issue):
        c_detail = jira.comment(issue.id, comment.id)
        if mr_url in c_detail.body and c_detail.author.name in JIRA_BOT_ACCOUNTS:
            LOGGER.info("Removing %s comment pointing to %s", issue.key, mr_url)
            c_detail.delete()


def remove_gitlab_link_in_issues(mr_id, namespace, issue_list):
    """Remove GitLab MR link from Issue Links field in JIRA Issues."""
    if not issue_list:
        return

    mr_link = f'{GITFORGE}/{namespace}/-/merge_requests/{mr_id}'
    jira = connect_jira()
    link = {}
    issues = _getissues(jira, issues=issue_list)
    for issue in issues:
        current_links = jira.remote_links(issue)
        link_exists = False
        for link in current_links:
            link_detail = jira.remote_link(issue=issue, id=link.id)
            if link_detail.object.url == mr_link:
                link_exists = True
                break
        if link_exists:
            LOGGER.info("MR %s linked in %s, removing it", mr_id, issue.key)
            jira.delete_remote_link(issue=issue, internal_id=link.id)
            remove_gitlab_link_comment_in_issue(jira, issue, mr_link)


def update_testable_builds(jissues, text, pipeline_urls):
    """Fill in Testable Builds field for the given JIRA Issues."""
    issue_list = [jissue.ji for jissue in jissues]
    ji_to_update = issues_with_lower_status(issue_list, JIStatus.READY_FOR_QA, JIStatus.IN_PROGRESS)
    LOGGER.info("Filling in Testable Builds for issues: %s", ji_to_update)
    LOGGER.debug("Text:\n%s\n", text)
    for issue in ji_to_update:
        testable_builds = getattr(issue.fields, JiraField.Testable_Builds)
        if testable_builds and all(url in testable_builds for url in pipeline_urls):
            LOGGER.info("All downstream pipelines found in %s, not updating field.", issue.key)
            continue
        if is_production_or_staging():
            issue.update(fields={JiraField.Testable_Builds: text})


def transition_issue(
        jira: JIRA,
        jissue: Union[str, int, Issue],
        jistatus: JIStatus,
        extra_transition_kwargs: Optional[dict] = None
        ) -> None:
    """Transition the issue to the given status."""
    status_str = jistatus.name.replace('_', ' ')
    if not is_production_or_staging():
        return
    jira.transition_issue(jissue, status_str, **extra_transition_kwargs)


def update_issue_status(issue_list, new_status, min_status=JIStatus.NEW):
    """Change the issue status to new_status if it is currently lower, otherwise do nothing."""
    # Do nothing if the current status is lower than min_status.
    # Returns the list of issue objects which have had their status changed.
    if not issue_list:
        LOGGER.info('No issues to update status for.')
        return []
    if new_status not in (JIStatus.IN_PROGRESS, JIStatus.READY_FOR_QA):
        LOGGER.warning("Unsupported transition status: %s", new_status.name)
        return []
    jira = connect_jira()
    if not (ji_to_update := issues_with_lower_status(issue_list, new_status, min_status)):
        LOGGER.info('All issues have status of %s or higher.', new_status.name)
        return []
    ji_keys = [ji.key for ji in ji_to_update]
    LOGGER.info('Updating status to %s for these issues: %s', new_status.name, ji_keys)
    if not is_production_or_staging():
        for issue in ji_to_update:
            issue.status = new_status.name
        return ji_to_update
    updated = []
    for issue in ji_to_update:
        if new_status is JIStatus.IN_PROGRESS:
            t_comment = f"GitLab kernel MR bot updated status to {new_status.name}"
            transition_issue(jira, issue, new_status, {'comment': t_comment})
            issue.fields.status = new_status.name
            updated.append(issue)
        if new_status is JIStatus.READY_FOR_QA:
            if prelim_testing := getattr(issue.fields, JiraField.Preliminary_Testing):
                if prelim_testing.value not in ('Pass', 'Requested'):
                    LOGGER.info("Setting Jira Issue's Preliminary Testing field to Requested")
                    issue.update(fields={JiraField.Preliminary_Testing: {'value': 'Requested'}})
    return updated


def get_linked_mrs(jissue_id, namespace=None, jiracon=None):
    """Return the list of MR urls linked to the given jira ID."""
    if not jiracon:
        jiracon = connect_jira()

    try:
        links = jiracon.remote_links(jissue_id)
    except JIRAError as err:
        LOGGER.warning("Jira returned an error fetching remote_links for '%s': %s", jissue_id,
                       err.text)
        return []
    starts_with = f'{GITFORGE}/{namespace}/' if namespace else f'{GITFORGE}/'
    return [link.object.url for link in links if link.object.url.startswith(starts_with)
            and '/-/merge_requests/' in link.object.url]
