# Metrics

## Purpose

Collect various metrics about infrastructure kwf depends on.

## Notifications

Prometheus alerts, that can be optionally configured.

## Manual Runs

For example, to run koji metrics:

```bash
CKI_METRICS_ENABLED=true \
    METRICS_CONFIG='{"default_enabled": false, "kojimetrics_enabled": true}' \
    CKI_LOGGING_LEVEL=DEBUG \
    KOJI_CHANNELS='{"brew":["rhel8-dupsign", "rhel9-dupsign"]}' \
    python3 -m webhook.metrics
```

## Updated Metrics

koji

1. monitor builders health in rhel8-dupsign and rhel9-dupsign channels
