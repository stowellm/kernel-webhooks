# Mergehook Webhook

## Purpose

This webhook verifies that a Merge Request can be merged.

## Notifications

This webhook will report the merge status of a Merge Request via a label,
forcing re-evaluation as needed when the MRs target branch is updated.

## Manual Runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.mergehook \
        --disable-inactive-branch-check \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.
